﻿using Eits.Domain.Users;
using System.Linq;

namespace Eits.DataAccess.UserMembership
{
    public class UserRegistrationDataAccess : BaseClasses.DataAccessBase
    {

        #region "Normal User Data Access"

        /// <summary>
        /// Normal User Data Access
        /// </summary>
        /// <param name="registerUser"></param>
        /// <returns></returns>
        public bool RegisterNormalUser(UserDetail registerUser)
        {
            var result = DbContext.Users.SingleOrDefault(x => x.UserName == registerUser.User.UserName);
            if (result != null) return false;

            DbContext.Users.Add(registerUser.User);

            DbContext.SaveChanges(0);
            registerUser.UserId = registerUser.User.UserId;

            DbContext.UserDetails.Add(registerUser);
            DbContext.SaveChanges(0);
            return true;
        }

        #endregion
    }
}
