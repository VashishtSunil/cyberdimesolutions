﻿

using Eits.Domain.Users;
using System.Linq;

namespace Eits.DataAccess.UserMembership
{
    public class UserLoginDataAccess : BaseClasses.DataAccessBase
    {
        #region"User Login Data Access"
        /// <summary>
        /// User login DataAccess Method
        /// </summary>
        /// <param name="userLoginDomain"></param>
        /// <returns></returns>
        public User UserLogin(User userLoginDomain)
        {
            return DbContext.Users.SingleOrDefault(x => x.UserName == userLoginDomain.UserName && x.UserPassword == userLoginDomain.UserPassword);
        }

        #endregion
    }
}
