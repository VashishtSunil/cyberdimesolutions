﻿using System.Linq;

namespace Eits.DataAccess.UserMembership
{
    public class RecoverPasswordDataAccess : BaseClasses.DataAccessBase
    {
        #region"Recover Password Data Access"

        /// <summary>
        /// Recovery Password DataAccess Method
        /// </summary>
        /// <param name="recoverPassword"></param>
        /// <returns></returns>
        public string RecoverPassword(Domain.UserMemberShip.RecoverPassword recoverPassword)
        {
            var result = DbContext.UserDetails.SingleOrDefault(x => x.User.UserName == recoverPassword.UserName);

            if (result == null) return "";
            return result.EmailAddress;
        }

        #endregion
    }
}
