﻿using Eits.Domain.Log;
using Eits.Domain.Users;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;

namespace Eits.DataAccess.DataLayer
{
    public class EitsEntityContext : DbContext
    {
        public EitsEntityContext() :
            base("name=DbConnectionString")
        {
            this.Configuration.LazyLoadingEnabled = false;
            Database.SetInitializer(new EitsInitializer());
        }
        public DbSet<UserRole> UserRoles { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<UserDetail> UserDetails { get; set; }
        public DbSet<ChangeLog> ChangeLogs { get; set; }

        public DbSet<Domain.World.Country> Country { get; set; }
        public DbSet<Domain.World.State> State { get; set; }
        public DbSet<Domain.MyContacts.Contacts> Contacts { get; set; }


       
        object GetPrimaryKeyValue(DbEntityEntry entry)
        {
            var objectStateEntry = ((IObjectContextAdapter)this).ObjectContext.ObjectStateManager.GetObjectStateEntry(entry.Entity);
            return objectStateEntry.EntityKey.EntityKeyValues[0].Value;
        }
        string GetPrimaryKeyName(DbEntityEntry entry)
        {
            var objectStateEntry = ((IObjectContextAdapter)this).ObjectContext.ObjectStateManager.GetObjectStateEntry(entry.Entity);
            if (objectStateEntry.EntityKey.EntityKeyValues[0].Key != null)
            {
                return objectStateEntry.EntityKey.EntityKeyValues[0].Key;
            }
            return "0";
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="logChanges"></param>
        /// <returns></returns>
        public int SaveChanges(int userId, bool logChanges = true)
        {
            //#region Change Log
            //if (logChanges)
            //{
            //    var list =
            //        ChangeTracker.Entries()
            //            .Where(
            //                t =>
            //                    t.State == EntityState.Added || t.State == EntityState.Deleted ||
            //                    t.State == EntityState.Modified);

            //    foreach (var item in list)
            //    {
            //        var entityName = item.Entity.GetType().Name;

            //        var originalValues = new Dictionary<string, string>();
            //        var newValues = new Dictionary<string, string>();

            //        var state = item.State;

            //        object primaryKeyValue = null;


            //        if (state != EntityState.Added)
            //        {
            //            primaryKeyValue = GetPrimaryKeyValue(item);
            //            //primaryKeyName = GetPrimaryKeyName(item);
            //        }

            //        switch (state)
            //        {
            //            case EntityState.Added:
            //                foreach (var propertyName in item.CurrentValues.PropertyNames)
            //                {
            //                    newValues.Add(propertyName, item.CurrentValues.GetValue<object>(propertyName) == null ? "" : item.CurrentValues.GetValue<object>(propertyName).ToString());
            //                }

            //                try
            //                {
            //                    base.SaveChanges();
            //                }

            //                catch (System.Data.Entity.Validation.DbEntityValidationException e)
            //                {
            //                    StringBuilder st = new StringBuilder();
            //                    foreach (var eve in e.EntityValidationErrors)
            //                    {

            //                        st.Append(string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:", eve.Entry.Entity.GetType().Name, eve.Entry.State));
            //                        foreach (var ve in eve.ValidationErrors)
            //                        {
            //                            st.Append(string.Format("- Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage));
            //                        }
            //                    }
            //                    //Utilities.ErrorLog.WriteError(st.ToString(), "");
            //                    throw;
            //                }
            //                catch (Exception ex)
            //                {

            //                }

            //                primaryKeyValue = GetPrimaryKeyValue(item);
            //                string primaryKeyName = GetPrimaryKeyName(item);

            //                newValues[primaryKeyName] = Convert.ToString(primaryKeyValue);
            //                break;

            //            case EntityState.Modified:

            //                var isDeleted = item.CurrentValues.GetValue<object>("IsDeleted") != null && (bool)item.CurrentValues.GetValue<object>("IsDeleted");
            //                if (isDeleted) state = EntityState.Deleted;

            //                DbEntityEntry item1 = item;
            //                foreach (var propertyName in item.CurrentValues.PropertyNames.Where(propertyName => !Equals(item1.OriginalValues.GetValue<object>(propertyName), item1.CurrentValues.GetValue<object>(propertyName))))
            //                {
            //                    originalValues.Add(propertyName,
            //                        item.OriginalValues.GetValue<object>(propertyName) == null
            //                            ? ""
            //                            : item.OriginalValues.GetValue<object>(propertyName).ToString());
            //                    newValues.Add(propertyName,
            //                        item.CurrentValues.GetValue<object>(propertyName) == null
            //                            ? ""
            //                            : item.CurrentValues.GetValue<object>(propertyName).ToString());
            //                }
            //                break;
            //        }

            //        if (newValues.Count <= 0 && originalValues.Count <= 0) continue;

            //        if (primaryKeyValue != null)
            //        {
            //            var log = new ChangeLog
            //            {
            //                FkRecordUpdatedBy = userId,
            //                CreatedDateTime = DateTime.Now,
            //                OriginalValue = DictionaryToJson(originalValues),
            //                NewValue = DictionaryToJson(newValues),
            //                RecordId = int.Parse(primaryKeyValue.ToString()),
            //                EventType = state.ToString(),
            //                Model = entityName
            //            };
            //            ChangeLogs.Add(log);
            //        }
            //    }
            //}
            //#endregion

            //try
            //{
            return base.SaveChanges();
            //}
            //catch (System.Data.Entity.Validation.DbEntityValidationException e)
            //{
            //    StringBuilder st = new StringBuilder();
            //    foreach (var eve in e.EntityValidationErrors)
            //    {
            //        st.Append(string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:", eve.Entry.Entity.GetType().Name, eve.Entry.State));
            //        foreach (var ve in eve.ValidationErrors)
            //        {
            //            st.Append(string.Format("- Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage));
            //        }
            //    }
            //    //Utilities.ErrorLog.WriteError(st.ToString(), "MoBigDataEntities - SaveChanges", Enums.Error.ErrorFiles.DbError.ToString());
            //    throw;
            //}
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dict"></param>
        /// <returns></returns>
        string DictionaryToJson(Dictionary<string, string> dict)
        {
            return Utilities.Conversion.JavaScriptTypeCast.DictionaryToJson(dict);
        }
    }
}
