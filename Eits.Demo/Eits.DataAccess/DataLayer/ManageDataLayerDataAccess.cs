﻿using System.Data.Entity;

namespace Eits.DataAccess.DataLayer
{
    public static class ManageDataLayerDataAccess
    {

        #region "Default Set Initializer"

        /// <summary>
        /// Default Set Initializer
        /// </summary>
        public static void SetInitializer()
        {
            Database.SetInitializer(new CreateDatabaseIfNotExists<EitsEntityContext>());
            //forcing to create database 
            using (EitsEntityContext obj = new EitsEntityContext())
            {
                obj.Database.Initialize(true);
            }
        }

        #endregion
    }
}
