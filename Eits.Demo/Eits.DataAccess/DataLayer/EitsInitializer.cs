﻿using Eits.Domain.Users;
using Eits.Domain.World;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Eits.DataAccess.DataLayer
{
    public class EitsInitializer : CreateDatabaseIfNotExists<EitsEntityContext>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        protected override void Seed(EitsEntityContext context)
        {
            #region Adding Roles
            IList<UserRole> defaultRoles = new List<UserRole>();
            defaultRoles.Add(new UserRole { UserRoleId = 1, RoleName = "SystemAdmin", RoleCode = "SA", RoleDescription = "Super admin of webiste", IsActive = true, IsDeleted = false, CreatedDateTime = DateTime.Now });
            defaultRoles.Add(new UserRole { UserRoleId = 2, RoleName = "NormalUser", RoleCode = "NU", RoleDescription = "Normal User registered using webiste", IsActive = true, IsDeleted = false, CreatedDateTime = DateTime.Now });
            foreach (UserRole std in defaultRoles)
            {
                context.UserRoles.Add(std);
            }
            #endregion

            #region Adding Countries and Its State
            var countries = new List<Country>
            {
                new Country {
                    CountryName ="Brazil",
                    LstCountryState =new List<State>
                    {
                        new State { StateName="Rio de Janeiro" },
                        new State { StateName="Ceara" },
                        new State { StateName="Santa Catarina" },
                        new State { StateName="Espirito Santo" },
                        new State { StateName="Bahia" },
                        new State { StateName="Sao Paulo" },
                        new State { StateName="Rio Grande do Sul" },
                        new State { StateName="Minas Gerais" },
                        new State { StateName="Para" },
                        new State { StateName="Parana" },
                        new State { StateName="Pernambuco" },
                        new State { StateName="Amazonas" },
                        new State { StateName="Distrito Federal" },
                        new State { StateName="Goias" },
                        new State { StateName="Sergipe" },
                        new State { StateName="Mato Grosso do Sul" },
                        new State { StateName="Rio Grande do Norte" },
                        new State { StateName="Paraiba" },
                        new State { StateName="Piaui" },
                        new State { StateName="Maranhao" },
                        new State { StateName="Mato Grosso" },
                        new State { StateName="Alagoas" },
                        new State { StateName="Tocantins" },
                        new State { StateName="Roraima" },
                        new State { StateName="Rondonia" },
                        new State { StateName="Amapa" },
                        new State { StateName="Acre" },
                    }                },
                new Country { CountryName="China",
                LstCountryState=new List<State>
                {
                    new State { StateName="Beijing" },
                    new State { StateName="Hebei" },
                    new State { StateName="Jiangsu" },
                    new State { StateName="Guangdong" },
                    new State { StateName="Zhejiang" },
                    new State { StateName="Liaoning" },
                    new State { StateName="Shanghai" },
                    new State { StateName="Tianjin" },
                    new State { StateName="Fujian" },
                    new State { StateName="Guangxi" },
                    new State { StateName="Shandong" },
                    new State { StateName="Chongqing" },
                    new State { StateName="Hubei" },
                    new State { StateName="Heilongjiang" },
                    new State { StateName="Shanxi" },
                    new State { StateName="Hunan" },
                    new State { StateName="Hainan" },
                    new State { StateName="Sichuan" },
                    new State { StateName="Henan" },
                    new State { StateName="Anhui" },
                    new State { StateName="Shaanxi" },
                    new State { StateName="Nei Mongol" },
                    new State { StateName="Jilin" },
                    new State { StateName="Guizhou" },
                    new State { StateName="Yunnan" },
                    new State { StateName="Jiangxi" },
                    new State { StateName="Gansu" },
                    new State { StateName="Xizang" },
                    new State { StateName="Ningxia" },
                    new State { StateName="Xinjiang" },
                    new State { StateName="Qinghai" },
                }                },
                new Country { CountryName="France",
                LstCountryState=new List<State>
                {
                   new State { StateName="Ile-de-3nce" },
                   new State { StateName="Midi-Pyrenees" },
                   new State { StateName="Picardie" },
                   new State { StateName="3nche-Comte" },
                   new State { StateName="Alsace" },
                   new State { StateName="Languedoc-Roussillon" },
                   new State { StateName="Centre" },
                   new State { StateName="Champagne-Ardenne" },
                   new State { StateName="Bretagne" },
                   new State { StateName="Rhone-Alpes" },
                   new State { StateName="Nord-Pas-de-Calais" },
                   new State { StateName="Lorraine" },
                   new State { StateName="Provence-Alpes-Cote d'Azur" },
                   new State { StateName="Limousin" },
                   new State { StateName="Haute-Normandie" },
                   new State { StateName="Poitou-Charentes" },
                   new State { StateName="Basse-Normandie" },
                   new State { StateName="Aquitaine" },
                   new State { StateName="Auvergne" },
                   new State { StateName="Pays de la Loire" },
                   new State { StateName="Bourgogne" },
                   new State { StateName="Corse" },
                }                },
                new Country { CountryName="India" ,
                LstCountryState=new List<State>                {
                   new State { StateName="Haryana" },
                   new State { StateName="Andhra Pradesh" },
                   new State { StateName="Delhi" },
                   new State { StateName="Tamil Nadu" },
                   new State { StateName="Uttar Pradesh" },
                   new State { StateName="Karnataka" },
                   new State { StateName="Maharashtra" },
                   new State { StateName="Kerala" },
                   new State { StateName="Chhattisgarh" },
                   new State { StateName="West Bengal" },
                   new State { StateName="Madhya Pradesh" },
                   new State { StateName="Gujarat" },
                   new State { StateName="Rajasthan" },
                   new State { StateName="Orissa" },
                   new State { StateName="Jharkhand" },
                   new State { StateName="Chandigarh" },
                   new State { StateName="Punjab" },
                   new State { StateName="Assam" },
                   new State { StateName="Daman and Diu" },
                   new State { StateName="Bihar" },
                   new State { StateName="Himachal Pradesh" },
                   new State { StateName="Uttarakhand" },
                   new State { StateName="Meghalaya" },
                   new State { StateName="Jammu and Kashmir" },
                   new State { StateName="Goa" },
                   new State { StateName="Puducherry" },
                   new State { StateName="Manipur" },
                   new State { StateName="Mizoram" },
                   new State { StateName="Dadra and Nagar Haveli" },
                   new State { StateName="Arunachal Pradesh" },
                   new State { StateName="Tripura" },
                   new State { StateName="Sikkim" },
                   new State { StateName="Nagaland" },
                } }                ,
                new Country { CountryName="USA" ,
                    LstCountryState =new List<State>
                {
                   new State { StateName="California" },
                   new State { StateName="Iowa" },
                   new State { StateName="New York" },
                   new State { StateName="New Jersey" },
                   new State { StateName="Massachusetts" },
                   new State { StateName="Connecticut" },
                   new State { StateName="Florida" },
                   new State { StateName="Texas" },
                   new State { StateName="Armed Forces US" },
                   new State { StateName="Tennessee" },
                   new State { StateName="Kentucky" },
                   new State { StateName="Georgia" },
                   new State { StateName="Illinois" },
                   new State { StateName="Colorado" },
                   new State { StateName="Utah" },
                   new State { StateName="Maryland" },
                   new State { StateName="South Carolina" },
                   new State { StateName="Montana" },
                   new State { StateName="Louisiana" },
                   new State { StateName="Washington" },
                   new State { StateName="Pennsylvania" },
                   new State { StateName="North Carolina" },
                   new State { StateName="Michigan" },
                   new State { StateName="Arkansas" },
                   new State { StateName="Wisconsin" },
                   new State { StateName="Ohio" },
                   new State { StateName="New Mexico" },
                   new State { StateName="Kansas" },
                   new State { StateName="Oregon" },
                   new State { StateName="Ne1ska" },
                   new State { StateName="West Virginia" },
                   new State { StateName="Virginia" },
                   new State { StateName="Missouri" },
                   new State { StateName="Mississippi" },
                   new State { StateName="Rhode Island" },
                   new State { StateName="4iana" },
                   new State { StateName="Oklahoma" },
                   new State { StateName="Minnesota" },
                   new State { StateName="Alabama" },
                   new State { StateName="Arizona" },
                   new State { StateName="South Dakota" },
                   new State { StateName="Nevada" },
                   new State { StateName="New Hampshire" },
                   new State { StateName="Maine" },
                   new State { StateName="Hawaii" },
                   new State { StateName="District of Columbia" },
                   new State { StateName="Delaware" },
                   new State { StateName="Idaho" },
                   new State { StateName="Wyoming" },
                   new State { StateName="North Dakota" },
                   new State { StateName="Vermont" },
                   new State { StateName="Alaska" },
                } }                ,
            };
            foreach (Country cnt in countries)
            {
                context.Country.Add(cnt);
            }
            #endregion



            context.SaveChanges(0);
        }
    }
}
