﻿using Eits.DataAccess.DataLayer;
using System;


namespace Eits.DataAccess.BaseClasses
{
    public abstract class DataAccessBase : IDisposable
    {
        protected internal EitsEntityContext DbContext;
        protected DataAccessBase()
        {
            DbContext = new EitsEntityContext();
        }


        /// <summary>
        /// 
        /// </summary>
        public void Dispose()
        {
            if (DbContext != null)
            {
                DbContext.Dispose();
            }
            DbContext = null;
        }
        /// <summary>
        /// 
        /// </summary>
        ~DataAccessBase()
        {
            Dispose();
        }
    }
}
