﻿using System;
using System.Linq;
using Eits.Domain.Users;
using System.Data.Entity;

namespace Eits.DataAccess.MyAccount
{
    public class MyAccountDataAccess : BaseClasses.DataAccessBase
    {
        /// <summary>
        /// Get Single Record of UserDetail By UserId
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public Domain.Users.UserDetail GetUserProfileDetails(int userId)
        {
            return DbContext.UserDetails.Single(x => x.UserId == userId);
        }
        /// <summary>
        /// Update Record of UserDetail
        /// </summary>
        /// <param name="userDetail"></param>
        public void UpdateUserProfileDetails(UserDetail userDetail)
        {
            DbContext.Entry(userDetail).State = EntityState.Modified;
            DbContext.SaveChanges(userDetail.UserId);          
        }
    }
}
