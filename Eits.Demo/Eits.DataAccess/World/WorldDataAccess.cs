﻿using Eits.Domain.World;
using System.Collections.Generic;
using System.Linq;


namespace Eits.DataAccess.World
{
    public class WorldDataAccess : BaseClasses.DataAccessBase
    {
        #region "World Data Access"
        /// <summary>
        /// Get All Country Details
        /// </summary>
        /// <returns></returns>
        public List<Country> GetCountry()
        {
            return DbContext.Country.Where(x => x.IsActive == true && x.IsDeleted == false).OrderBy(x => x.CountryName).ToList();
        }
        /// <summary>
        /// Get Related States of Country By CountryId
        /// </summary>
        /// <param name="countryId"></param>
        /// <returns></returns>
        public List<State> GetState(int countryId)
        {
            return DbContext.State.Where(x => x.FKCountryId == countryId && x.IsActive == true && x.IsDeleted == false).OrderBy(x => x.StateName).ToList();
        }

        #endregion
    }
}
