﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
namespace Eits.DataAccess.Contacts
{
    public class ManageContactsDataAccess : BaseClasses.DataAccessBase
    {
                #region"Manage Contact Data Access Method"

        /// <summary>
        /// Get List of Contact Records 
        /// </summary>
        /// <param name="fkCreatedBy"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageNumber"></param>
        /// <returns></returns>
        public List<Domain.MyContacts.Contacts> GetAllContacts(int fkCreatedBy,int pageSize,int skip)

        {
           
            var query = (from a in DbContext.Contacts
                         join b in DbContext.State on a.FkStateId equals b.StateId
                         join c in DbContext.Country on b.FKCountryId equals c.CountryId
                         where a.IsDeleted == false && a.FKCreatedBy == fkCreatedBy
                         select new
                         {
                             ContactId = a.ContactId,
                             ContactPerson = a.ContactPerson,
                             ContactNo = a.ContactNo,
                             StateName = b.StateName,
                             CountryName = c.CountryName
                         });


            return query.OrderBy(a => a.ContactId).Skip(skip).Take(pageSize).ToList().Select(r => new Domain.MyContacts.Contacts
            {
                TotalRecords = query.Count(),
                ContactId = r.ContactId,
                ContactPerson = r.ContactPerson,
                ContactNo = r.ContactNo,
                ContactState = new Domain.World.State
                {
                    StateName = r.StateName,
                    StateCountry = new Domain.World.Country
                    {
                        CountryName = r.CountryName
                    }
                },

            }).ToList();
        }
        /// <summary>
        /// Add new contact
        /// </summary>
        /// <param name="contacts"></param>
        public void AddNewContact(Domain.MyContacts.Contacts contacts)
        {
            DbContext.Contacts.Add(contacts);
            //DbContext.Entry(contacts.ContactState).State = EntityState.Unchanged;
            DbContext.SaveChanges();
        }
        /// <summary>
        /// Get Single Contact Record By ID
        /// </summary>
        /// <param name="contactId"></param>
        /// <param name="fkCreatedBy"></param>
        /// <returns></returns>
        public Domain.MyContacts.Contacts GetContactDetailsById(int contactId, int fkCreatedBy)
        {
            return DbContext.Contacts.Include(mbox => mbox.ContactState).Where(x => x.FKCreatedBy == fkCreatedBy && x.ContactId == contactId).Single();
        }
        /// <summary>
        /// Update Single Contact Record 
        /// </summary>
        /// <param name="contacts"></param>
        public void UpdateContactDetails(Domain.MyContacts.Contacts contacts)
        {
            var c = DbContext.Contacts.Single(x => x.ContactId == contacts.ContactId);
            c.ContactNo = contacts.ContactNo;
            c.ContactPerson = contacts.ContactPerson;
            c.FkStateId = contacts.FkStateId;
            DbContext.Entry(c).State = EntityState.Modified;
            DbContext.SaveChanges(contacts.FkUpdatedBy);
        }
        /// <summary>
        /// Get Single Contact Record For Delete operation.
        /// </summary>
        /// <param name="contactId"></param>
        /// <returns></returns>
        public Domain.MyContacts.Contacts GetContactByDeleteConfirmation(int contactId)
        {
            var query = (from a in DbContext.Contacts
                         join b in DbContext.State on a.FkStateId equals b.StateId
                         join c in DbContext.Country on b.FKCountryId equals c.CountryId
                         where a.IsDeleted == false && a.ContactId == contactId
                         select new
                         {
                             ContactId = a.ContactId,
                             ContactPerson = a.ContactPerson,
                             ContactNo = a.ContactNo,
                             StateName = b.StateName,
                             CountryName = c.CountryName
                         });
            return query.AsEnumerable().Select(r => new Domain.MyContacts.Contacts
            {
                ContactId = r.ContactId,
                ContactPerson = r.ContactPerson,
                ContactNo = r.ContactNo,
                ContactState = new Domain.World.State
                {
                    StateName = r.StateName,
                    StateCountry = new Domain.World.Country
                    {
                        CountryName = r.CountryName
                    }
                },

            }).Single();

        }
        /// <summary>
        /// Delete the Single contact Record.
        /// </summary>
        /// <param name="contactId"></param>
        public void  DeleteSingleContact(int contactId)
        {
            var data = DbContext.Contacts.Single(x => x.ContactId == contactId);
            data.IsDeleted = true;
            DbContext.SaveChanges();
          
        }

        #endregion
    }
}
