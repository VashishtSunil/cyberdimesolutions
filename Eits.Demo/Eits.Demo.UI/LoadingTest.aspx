﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LoadingTest.aspx.cs" Inherits="Eits.Demo.UI.LoadingTest" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css" />
  <link rel="stylesheet" href="/resources/demos/style.css" />
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
  <script type="text/javascript" language="javascript">    
       $(document).ready(function () {    
           // jquery Progress bar function.    
           $("#progressbar").progressbar({ value: 0 });    
           $("#lbldisp").hide();        
               //button click event    
               $("#btnGetData").click(function () {    
               $("#btnGetData").attr("disabled", "disabled")    
               $("#lbldisp").show();    
               //call back function    
               var intervalID = setInterval(updateProgress, 250);    
               $.ajax({    
                   type: "POST",    
                   url: "LoadingTest.aspx/GetText",    
                   data: "{}",    
                   contentType: "application/json; charset=utf-8",    
                   dataType: "json",    
                   async: true,    
                   success: function (msg) {    
                       $("#progressbar").progressbar("value", 100);    
                       $("#lblStatus").hide();    
                       $("#lbldisp").hide();    
                       $("#result").text(msg.d);                            
                       clearInterval(intervalID);    
                   }    
               });    
               return false;    
           });    
       });    
///This function is the callback function which is executed in every 250 milli seconds. Until the ajax call is success this method is invoked and the progressbar value is changed.   
       function updateProgress() {               
           var value = $("#progressbar").progressbar("option", "value");    
           if (value < 100) {    
               $("#progressbar").progressbar("value", value + 1);    
               $("#lblStatus").text((value + 1).toString() +"%");                
           }    
       }                   
   </script>  
</head>
<body>
    <form id="form1" runat="server">
   <div id="progressbar"></div>    
   <div id="result"></div><br />    
   <asp:Label runat="server"  ID="lbldisp" Text= "Percentage Completed : "/>    
   <asp:Label   runat="server" ID="lblStatus" />    
   <br />    
   <asp:Button ID="btnGetData" runat="server" Text="Get Data" />  
    </form>
</body>
</html>
