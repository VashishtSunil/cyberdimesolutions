﻿using System.Web.Optimization;

namespace Eits.Demo.UI
{
    public class BundleConfig
    {
                public static void RegisterBundles(BundleCollection bundles)
        {
            /*for not logged in users*/
            bundles.Add(new ScriptBundle("~/js").Include(
                      "~/Templates/resources/js/jquery/jquery-1.11.1.js",
                      // "~/Templates/resources/js/jquery/jquery-ui-1.11.4.js",
                      "~/Templates/resources/bootstrap/js/bootstrap.js",
                     //Validation Js Starts
                     "~/Templates/resources/js/validation/jquery.validate.js",
                     "~/Templates/resources/js/validation/jquery.validate.unobtrusive.js",
                     "~/Templates/resources/js/validation/jquery-migrate-1.1.0.js",
              //Validation Js Ends

              //sweet alert starts
              "~/Templates/resources/js/Plugin/AlertMessage/sweetalert-dev.js",
             //sweet alert ends

             "~/Templates/resources/js/*.js"

                      ));

            /*for  logged in users*/
            bundles.Add(new ScriptBundle("~/Ljs").Include(
                      "~/Templates/resources/js/jquery/jquery-1.11.1.js",
                      //"~/Templates/resources/js/jquery/jquery-ui-1.11.4.js",
                      "~/Templates/resources/bootstrap/js/bootstrap.js",
                     //Validation Js Starts
                     "~/Templates/resources/js/validation/jquery.validate.js",
                     "~/Templates/resources/js/validation/jquery.validate.unobtrusive.js",
                     "~/Templates/resources/js/validation/jquery-migrate-1.1.0.js",
              //Validation Js Ends

              //sweet alert starts
              "~/Templates/resources/js/Plugin/AlertMessage/sweetalert-dev.js",
             //sweet alert ends

             "~/Templates/resources/js/*.js",

             "~/Templates/resources/js/secureScripts/*.js"

                      ));




            bundles.Add(new StyleBundle("~/css").Include(
                        "~/Templates/resources/bootstrap/css/bootstrap-theme.css",
                        "~/Templates/resources/bootstrap/css/bootstrap.css",

                        //font-awesome-4.6.3 starts
                        "~/Templates/resources/font-awesome-4.6.3/css/font-awesome.css",
                //font-awesome-4.6.3 ends

                //sweet alert starts
                "~/Templates/resources/js/Plugin/AlertMessage/sweetalert.css",
                //sweet alert ends

                //other css
                "~/Templates/resources/css/*.css"

                        // "~/Templates/resources/js/jquery/jquery-ui-1.11.4.css"

                        ));
        }
    }
}
