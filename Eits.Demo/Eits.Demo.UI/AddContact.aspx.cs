﻿using Eits.DomainLogic.Contacts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Eits.Demo.UI
{
    public partial class AddContact : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                GetCountries();
            }
        }

        protected void btn_save_Click(object sender, EventArgs e)
        {
            Domain.MyContacts.Contacts contDetail = new Domain.MyContacts.Contacts();
            contDetail.ContactPerson = txt_contactName.Text;
            contDetail.ContactNo = txt_contactNumber.Text;
            contDetail.FkStateId = ddl_states.SelectedItem.Value.ToType<int>();
            ManageContactsDomainLogic obj = new ManageContactsDomainLogic();
            obj.AddNewContact(contDetail);



        }

        public void GetCountries()
        {
            
            ddl_countries.DataSource = DomainLogic.World.WorldDomainLogic.GetCountry();
            ddl_countries.DataTextField = "CountryName";
            ddl_countries.DataValueField = "CountryId";
            ddl_countries.DataBind();
            

        }

        protected void ddl_countries_SelectedIndexChanged(object sender, EventArgs e)
        {
            binState(ddl_countries.SelectedValue.ToType<int>());

        }

        public void binState(int countryId)
        {
            ddl_states.DataSource = DomainLogic.World.WorldDomainLogic.GetState(countryId);
            ddl_states.DataTextField = "StateName";
            ddl_states.DataValueField = "StateId";
            ddl_states.DataBind();
        }
    }
}