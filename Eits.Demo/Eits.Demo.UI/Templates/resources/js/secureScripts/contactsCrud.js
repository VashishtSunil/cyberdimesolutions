﻿var contacts_panel;
var pageIndex = 0;
var $userContactListTable;


$(document).ready(function () {
    contacts_panel = $("#contacts_panel");
    if (contacts_panel.length <= 0) return;
    //Populate Contact
    LoadContacts(pageIndex);

    $('body').on('change', '#ContactCountryId', function () {
        var countryId = $(this).val();
        LoadStates(countryId);
    });
  
 
});

/*load contacts*/
function LoadContacts(pageIndex) {
  
    GetData("/contacts/getcontacts", { pageIndex: pageIndex, pagesize: GetPageSize() }, LoadContactsSuccess);
}


/*Get All Contact Details & making Table */
function LoadContactsSuccess(d) {
 
    if (d.length > 0) {
      
        if ($("#userContactsList").length == 0) {
            //adding header to Table
            $userContactListTable = $('<table id="userContactsList" class="table"></table>').addClass('table table-responsive table-striped');
            var header = "<thead><tr><th>Contact Person</th><th>Contact No</th><th>State</th><th>Country</th><th>Actions</th></tr></thead>";
            $userContactListTable.append(header);
        }

        LoadRowContact(d);
     
       
    }
    else {
        var $noData = $('<div/>').html('No Data Found!');
        $("#tg_LoadMore").hide();
        $("#ddl_pageRange").hide();
        $("#lblpagesize").hide();
        contacts_panel.html($noData);
    }
}
/* Load the Contacts in Rows */
function LoadRowContact(d) {
   
    var totalrecord = 0;
    var remainigrecord = 0;
    $.each(d, function (i, row) {
        var $row = $('<tr/>');
        $row.append($('<td/>').html(row.ContactPerson));
        $row.append($('<td/>').html(row.ContactNo));
        $row.append($('<td/>').html(row.ContactState.StateName));
        $row.append($('<td/>').html(row.ContactState.StateCountry.CountryName));
        $row.append($('<td/>').html("<a href='/contacts/savecontact/" + row.ContactId + "' class='popup' id='editcontact' >Edit</a>&nbsp; <a href='/contacts/deleteconfirmation/" + row.ContactId + "' class='popup' id='deletecontact'>Delete</a>&nbsp; "));
        $userContactListTable.append($row);
        totalrecord = row.TotalRecords;
    }); 
    pageIndex = parseInt(pageIndex + 1);
    pageIndex = pageIndex;
    var getTotalRecord = parseInt(totalrecord);

    if (getTotalRecord == $userContactListTable.find("tbody tr").length) {
        $("#tg_LoadMore").hide();
    }
    else {
        $("#tg_LoadMore").show();
    }  
    contacts_panel.html($userContactListTable);
}


///*Get Page Size on Drop Down*/
$('#ddl_pageRange').on('change', function () {
  
    $("#userContactsList").remove();
    LoadContacts(0);
    pageIndex = 0;
});

function GetPageSize() {
  return $('#ddl_pageRange').val();
}

/*Load Contacts on LoadMore Click Button */
function LoadMoreClick() {

   LoadContacts(pageIndex);
  
}

//Casecade dropdown - Populate states of selected country
function LoadStates(countryId) {
    var $state = $('#FkStateId');
    $state.empty();;
    $state.append($('<option></option>').val('').html('Please Wait...'));
    if (countryId == null || countryId == "") {
        $state.empty();
        $state.append($('<option></option>').val('').html('Select State'));
        return;
    }
    GetData("/contacts/getstatelist", { countryID: countryId }, LoadStatesSuccess);
}
function LoadStatesSuccess(result) {
    var $state = $('#FkStateId');
    $state.empty();
    $state.append($('<option></option>').val('').html('Select State'));
    $.each(result, function (i, val) {
        $state.append($('<option></option>').val(val.StateId).html(val.StateName));
    });
}

/*Add new contact success starts*/
function AddNewContactSucess(result) {
    if (result.IsSuccess == false) {
        errorBlock(result.OutPutMessage);
    } else {
        $("#userContactsList").remove();
        pageIndex = 0;
        LoadContacts(pageIndex);
        success(result.OutPutMessage);
        closeDialog();
    }
}
/*Add new contact success ends*/

/*Delete contact success Starts*/
function DeleteContactSuccess() {
    $("#userContactsList").remove();
  
    success("Contact deleted successfully");
    pageIndex = 0;
    LoadContacts(pageIndex);
    closeDialog();
}

/*Close Message PopUp*/
function CancelDialogue()
{
    closeDialog();
}



/*Upload a File from EditProfile*/
$('#btn_upload').click(function () {

    if ($("#fileUpload").val() == '')
    {
        errorBlock("Please choose Image");
        return false;
    }

    $('input[type="button"]').prop('disabled', true);
    UploadUserProfile();
});

/*Increase the percentage of progress loading */
function updateProgress(evt) {


    if (evt.lengthComputable) {  //evt.loaded the bytes browser receive
        //evt.total the total bytes seted by the header
        //
        $(".progress").css("display", "block");
        var percentComplete = (evt.loaded / evt.total) * 100;
        $('.percent').html(parseInt(percentComplete) + "%");
        $('.bar').width(parseInt(percentComplete) + "%")
    }
}
/*Validation for Fileupload */
$("#fileUpload").change(function () {
    var fileExtension = ['jpeg', 'jpg', 'png', 'gif'];
    if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
        errorBlock("Only '.jpeg','.jpg', '.png', '.gif' formats are allowed.");
        $('input[type="button"]').prop('disabled', true);
    }
    else {
      
        $('input[type="button"]').prop('disabled', false);
    }
 
});

/*Function of upload Image */
function UploadUserProfile() {
    var xhr = new XMLHttpRequest();
    var data = new FormData();
    var files = $('#fileUpload').get(0).files;
    for (var i = 0; i < files.length; i++) {
        data.append(files[i].name, files[i]);
    }
    xhr.upload.addEventListener("progress", updateProgress, false);
    xhr.open("POST", "/myaccount/UploadFiles");
    xhr.onreadystatechange = function (e) {

        if (xhr.readyState == 4) {

            var result11223 = xhr.response.valueOf();
          
            $(".progress").css("display", "none");

            if (result11223 == "2")
            {
                errorBlock('Extension Support  *.jpg ,*.jpeg ,*.png ,*gif');
            }
            else if (result11223 == "3")
            {
                errorBlock("File size can not more than 5MB");
            }
           else 
            {
                $("#prev_Img").prop("src", "/Content/UploadImages/" + result11223);
              
                success('Your Image is successfully Loaded.');
              
            }
           
           
        }
        $('#btn_upload').attr("disabled", false);
    }
    xhr.send(data);

}
