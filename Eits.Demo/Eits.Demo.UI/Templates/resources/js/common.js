﻿$(document).ready(function () {
    //Open popup
    $('body').on("click", "a.popup", function (e) {
        e.preventDefault();
        var page = $(this).attr('href');
        OpenPopup(page);
    });
});

/************************************** *Trim Function Starts***************************************/
function trim(stringToTrim) {
    if (!isStringValid(stringToTrim)) return "";
    return stringToTrim.replace(/^\s+|\s+$/g, "");
}
function ltrim(stringToTrim) {
    if (!isStringValid(stringToTrim)) return "";
    return stringToTrim.replace(/^\s+/, "");
}
function rtrim(stringToTrim) {
    if (!isStringValid(stringToTrim)) return "";
    return stringToTrim.replace(/\s+$/, "");
}
function isStringValid(str) {
    if (str == "") return false;
    if (str == undefined) return false;
    return true;
}
/************************************** *Trim Function Ends***************************************/


/********************************* Show messages starts *********************************/
function errorBlock(msg) {
    sweetAlert("Error !!!", msg, "error");
}
function success(msg) {
    sweetAlert("Success", msg, "success");
}
/********************************* Show messages Ends *********************************/

/********************************* Model pop-up loading starts *********************************/
$(function () {
    // Initialize numeric spinner input boxes
    //$(".numeric-spinner").spinedit();
    // Initialize modal dialog
    // attach modal-container bootstrap attributes to links with .modal-link class.
    // when a link is clicked with these attributes, bootstrap will display the href content in a modal dialog.
    $('body').on('click', '.modal-link', function (e) {
        e.preventDefault();
        $(this).attr('data-target', '#modal-container');
        $(this).attr('data-toggle', 'modal');
    });
    // Attach listener to .modal-close-btn's so that when the button is pressed the modal dialog disappears
    $('body').on('click', '.modal-close-btn', function () {
        $('#modal-container').modal('hide');
    });
    //clear modal cache, so that new content can be loaded
    $('#modal-container').on('hidden.bs.modal', function () {
        $(this).removeData('bs.modal');
    });
    $('#CancelModal').on('click', function () {
        return false;
    });
});
/********************************* Model pop-up loading Ends *********************************/


/*Display Password of Text box starts*/
$(".reveal").mousedown(function () {
    $(".pwd").replaceWith($('.pwd').clone().attr('type', 'text'));
})
.mouseup(function () {
    $(".pwd").replaceWith($('.pwd').clone().attr('type', 'password'));
})
.mouseout(function () {
    $(".pwd").replaceWith($('.pwd').clone().attr('type', 'password'));
});
/*Display Password of Text box Ends*/

/************************************** *AJAX Custom POST Starts***************************************/
function PostData(url, data, successHandler) {
    $.ajax({
        type: 'POST',
        url: url,
        data: data,
        success: successHandler
        
    });
}
/************************************** *AJAX Custom POST Ends***************************************/

/************************************** *AJAX Custom POST Starts***************************************/
function GetData(url, data, successHandler) {
     
    $.ajax({
        type: 'GET',
        url: url,
        data: data,
        dataType: 'json',
        success: successHandler
    });
}
/************************************** *AJAX Custom POST Ends***************************************/

/**************************************open popup starts**************************************/
function closeDialog() {
    if ($dialog == null) return;
     
        $dialog.dialog('close');
     
    
}

var $dialog;
function OpenPopup(page) {
    //closeDialog();
    var $pageContent = $('<div/>');
    $pageContent.load(page);
    $dialog = $('<div class="popupWindow" style="overflow:hidden"></div>')
        .html($pageContent)
        .dialog({
            draggable: false,
            autoOpen: false,
            resizable: false,
            model: true,
            height: 600,
            width: 600,
            close: function () {
                $dialog.dialog('destroy').remove();
            }
        });
    $dialog.dialog('open');
}
/**************************************open popup Ends**************************************/

/************************************** *AJAX POST Starts***************************************/
$(function () {
    $("form[data-post-type='ajax'][ method=\"post\"]").find(':submit').live("click", function () {
        var form = $(this).parents('form:first');
        $.validator.unobtrusive.parse(form);
        var isValid = form.validate().form();
        //alert(isValid);
        if (isValid) {
            form.css("opacity", "0.2");
            $.ajax({
                type: 'POST',
                url: form.attr('action'),
                data: form.serialize(),
                success: function (data) {
                    form.css("opacity", "1");
                    var functionName = form.data('success-function');
                    if ((trim(functionName)) != "") {
                        window[functionName](data, form);
                    }
                }
            });
            return false;
        }
        return false;
    });
});

/************************************** *AJAX POST Ends***************************************/