﻿using Eits.Domain.Users;
using Eits.DomainLogic.MyAccount;
using System;
using System.IO;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace Eits.Demo.UI.Controllers.MyAccount
{
    public class MyAccountController : BaseClasses.AuthoriseUserControllerBase
    {
        /// <summary>
        ///  Edit Profile  Get Method
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult EditProfile()
        {
            MyAccountDomainLogic obj = new MyAccountDomainLogic();
            Domain.Users.UserDetail data = obj.GetUserProfileDetails();
            Session["userDetail"] = data;
            return View(data);
        }
        /// <summary>
        ///  Edit profile post method
        /// </summary>
        /// <returns></returns>
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult EditProfile(Domain.Users.UserDetail userDetail)
        {
            if(ModelState.IsValid)
            {
              
                MyAccountDomainLogic obj = new MyAccountDomainLogic();
                UserDetail usr = (UserDetail)TempData["ImageDetail"];
                if(usr != null)
                {
                    userDetail.OrignalFileName = usr.OrignalFileName;
                    userDetail.FileName = usr.FileName;

                }

                obj.UpdateUserProfileDetails(userDetail);
                Success("Profile details updated successfully");
            }            
            return View(userDetail);
        }


        /// <summary>
        ///  Logout Method
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult LogOut()
        {
            FormsAuthentication.SignOut();
            return RedirectAfterLogOut();
        }


      

        /// <summary>
        ///  File Upload method
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public string  UploadFiles()
        {
            string result = "0";
            try
            {
                HttpFileCollectionBase files = Request.Files;
                if (files.Count > 0)
                {
                    foreach (string key in files)
                    {

                        HttpPostedFileBase file = files[key];

                        if (file.ContentLength > (10240000 / 2))
                        {
                            result = "3";
                        }
                        else
                        {  
                            string fileName = file.FileName;

                            string newFileName = "";

                            string file_ext = Path.GetExtension(fileName).ToLower();

                            if (file_ext == ".jpeg" ||  file_ext == ".jpg"  || file_ext == ".gif"  || file_ext == ".png")
                            {
                                newFileName = DateTime.Now.ToFileTimeUtc().ToString();
                                newFileName = newFileName + file_ext;
                                string filepath = Server.MapPath("~/Content/UploadImages/" + newFileName);
                                file.SaveAs(filepath);

                                if (Session["userDetail"] != null)
                                {
                                    UserDetail data = (UserDetail)HttpContext.Session["userDetail"];
                                    string existingFileName = data.FileName;
                                    string FileNamepath = Server.MapPath("~/Content/UploadImages/" + existingFileName);
                                    FileInfo fileinfo = new FileInfo(FileNamepath);
                                    if (fileinfo.Exists)
                                    {
                                        fileinfo.Delete();
                                    }
                                    data.OrignalFileName = fileName;
                                    data.FileName = newFileName;
                                    MyAccountDomainLogic obj = new MyAccountDomainLogic();
                                    obj.UpdateUserProfileDetails(data);

                                     TempData["ImageDetail"] = data;

                                    result = newFileName;
                                    Thread.Sleep(1);
                                }
                             

                            }
                            else
                            {

                                result = "2"; // Invalid File Format  
                            }




                        }
                    }
                }

               
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}