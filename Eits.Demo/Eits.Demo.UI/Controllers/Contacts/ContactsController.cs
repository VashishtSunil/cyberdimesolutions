﻿using Eits.Domain.Users;
using Eits.DomainLogic.Contacts;
using Eits.DomainLogic.World;
using System.Web.Mvc;

namespace Eits.Demo.UI.Controllers.Contacts
{
    public class ContactsController : BaseClasses.AuthoriseUserControllerBase
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult MyContacts()
        {
            return View();
        }
        /// <summary>
        /// Get All Contact Records
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult GetContacts(int pageIndex = 0, int pagesize = 0 )
        {
            ManageContactsDomainLogic obj = new ManageContactsDomainLogic();
            return new JsonResult { Data = obj.GetAllContacts(pageIndex, pagesize), JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        /// <summary>
        /// Get Single Contact Record By Id For Updation.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult SaveContact(int id = 0)
        {
            Domain.MyContacts.Contacts result;
            if (id > 0)
            {
                ManageContactsDomainLogic obj = new ManageContactsDomainLogic();
                result = obj.GetContactDetailsById(id);
                result.LstState = WorldDomainLogic.GetState(result.ContactState.FKCountryId);
            }
            else
            {
                result = new Domain.MyContacts.Contacts();
            }
            result.LstCountry = WorldDomainLogic.GetCountry();
            
            return View(result);
        }
        /// <summary>
        /// Get States By country Id
        /// </summary>
        /// <param name="countryId"></param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult GetStateList(int countryId)
        {
            return new JsonResult
            {
                Data = WorldDomainLogic.GetState(countryId),
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }
        /// <summary>
        ///Update Single Contact Record 
        /// </summary>
        /// <param name="contacts"></param>

        /// <returns></returns>
        [ValidateAntiForgeryToken]
        [HttpPost]
        public JsonResult SaveContact(Domain.MyContacts.Contacts contacts)
        {
            if (ModelState.IsValid)
            {
                ManageContactsDomainLogic obj = new ManageContactsDomainLogic();
                if (contacts.ContactId > 0)
                {
                    
                    
                    obj.UpdateContactDetails(contacts);
                }
                else
                {
                    obj.AddNewContact(contacts);
                }
                return ReturnAjaxSuccessMessage("Contact saved successfully");
            }
            return ReturnAjaxModelError();
        }

        /// <summary>
        /// Get Single Contact For Delete Operation
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult DeleteConfirmation(int id)
        {
            ManageContactsDomainLogic obj = new ManageContactsDomainLogic();
            return View(obj.GetContactByDeleteConfirmation(id));
        }
        /// <summary>
        /// Delete Single Contact 
        /// </summary>
        /// <param name="id"></param>
        [HttpPost]
        public void DeleteSingleContact(int id)
        {
            ManageContactsDomainLogic obj = new ManageContactsDomainLogic();
            obj.DeleteSingleContact(id);
        }


    }
}