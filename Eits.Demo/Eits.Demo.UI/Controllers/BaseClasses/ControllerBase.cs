﻿using System.Linq;
using System.Web.Mvc;

namespace Eits.Demo.UI.Controllers.BaseClasses
{
    public abstract class ControllerBase : Controller
    {
        #region Ajax Messages
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public JsonResult ReturnAjaxModelError()
        {
            return Json(new
            {
                IsSuccess = false,
                OutPutMessage = string.Join("<br/>", ModelState.Keys.SelectMany(k => ModelState[k].Errors)
                                .Select(m => m.ErrorMessage).ToArray())
            });
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public JsonResult ReturnAjaxErrorMessage(string errorMessage)
        {
            return Json(new
            {
                IsSuccess = false,
                OutPutMessage = errorMessage
            });
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="successMessage"></param>
        /// <returns></returns>
        public JsonResult ReturnAjaxSuccessMessage(string successMessage)
        {
            return Json(new
            {
                IsSuccess = true,
                OutPutMessage = successMessage
            });
        }
        #endregion

        #region Message Functions
        /// <summary>
        /// For Error message that stays until we close
        /// </summary>
        /// <param name="msg"></param>
        protected void ErrorBlock(string msg)
        {
            ViewBag.errorBlock = msg;
        }
        /// <summary>
        /// success message closed after 5000 millisecond
        /// </summary>
        /// <param name="msg"></param>
        protected void Success(string msg)
        {
            ViewBag.success = msg;
        }
        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="returnUrl"></param>
        /// <returns></returns>
        protected ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("userlogin", "usermembership");
        }
    }
}