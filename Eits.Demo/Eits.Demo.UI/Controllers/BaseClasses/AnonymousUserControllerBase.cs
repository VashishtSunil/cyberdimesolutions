﻿using System.Web.Mvc;

namespace Eits.Demo.UI.Controllers.BaseClasses
{
    public abstract class AnonymousUserControllerBase : ControllerBase
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="returnUrl"></param>
        /// <returns></returns>
        protected ActionResult RedirectAfterLogin(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("editprofile", "myaccount");
        }
        
    }
}