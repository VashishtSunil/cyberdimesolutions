﻿using Eits.Domain.Users;
using Eits.DomainLogic.UserMembership;
using System.Web.Mvc;

namespace Eits.Demo.UI.Controllers.UserMembership
{
    public partial class UserMemberShipController
    {
        /// <summary>
        ///  Register Get Method
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Register()
        {
            return View(new UserDetail());
        }


        /// <summary>
        ///  Register Post Method
        /// </summary>
        /// <returns></returns>
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Register(UserDetail registerUser)
        {
            if (ModelState.IsValid)
            {
                UserRegistrationDomainLogic obj = new UserRegistrationDomainLogic();
                if (!obj.RegisterNormalUser(registerUser))
                {
                    ErrorBlock("Username already exists");
                }
                else
                {
                    UserLoginDomainLogic objUserLogin = new UserLoginDomainLogic();
                    objUserLogin.ForceFullUserLogin(new User
                    {
                        UserName = registerUser.User.UserName,
                        UserRoleId = registerUser.User.UserRoleId,
                        UserId = registerUser.UserId
                    });
                    return RedirectAfterLogin("");
                }
            }
            return View(registerUser);
        }

        /// <summary>
        /// Term and Condition Get Method
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult _TermsAndConditions()
        {
            return PartialView("_TermsAndConditions");
        }
    }
}