﻿using Eits.Domain.UserMemberShip;
using Eits.DomainLogic.UserMembership;
using System.Web.Mvc;
namespace Eits.Demo.UI.Controllers.UserMembership
{
    public partial class UserMemberShipController
    {
        /// <summary>
        ///  Recover Password Action Get Method
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult RecoverPassword()
        {
            return View(new RecoverPassword());
        }

        /// <summary>
        /// Recover Password Action Post Method
        /// </summary>
        /// <param name="recoverPassword"></param>
        /// <returns></returns>
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult RecoverPassword(RecoverPassword recoverPassword)
        {
            if (!ModelState.IsValid) return View(recoverPassword);
            RecoverPasswordDomainLogic obj = new RecoverPasswordDomainLogic();
            if (obj.RecoverPassword(recoverPassword))
            {
                Success("Instructions to setup new password is sent to your email address");
                ModelState.Clear();
                recoverPassword = new RecoverPassword();
            }
            else
            {
                ErrorBlock("Username does not exists");
            }
            return View(recoverPassword);
        }
    }
}