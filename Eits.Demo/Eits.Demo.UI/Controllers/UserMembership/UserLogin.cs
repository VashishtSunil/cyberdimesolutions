﻿
using Eits.Domain.Users;
using Eits.DomainLogic.UserMembership;
using System.Web.Mvc;

namespace Eits.Demo.UI.Controllers.UserMembership
{
    public partial class UserMemberShipController
    {
        /// <summary>
        /// User login Get method
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult UserLogin()
        {
            return View(new User());
        }

        /// <summary>
        /// User Login Post Method
        /// </summary>
        /// <param name="userlogin"></param>
        /// <param name="returnUrl"></param>
        /// <returns></returns>
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult UserLogin(User userlogin, string returnUrl)
        {
            if (!ModelState.IsValid) return View(userlogin);
            UserLoginDomainLogic obj = new UserLoginDomainLogic();
            string result = obj.UserLogin(userlogin);
            if (!string.IsNullOrEmpty(result))
            {
                ErrorBlock(result);
            }
            else
            {
                return RedirectAfterLogin(returnUrl);
            }
            return View(userlogin);
        }
    }
}