﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddContact.aspx.cs" Inherits="Eits.Demo.UI.AddContact" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:TextBox ID="txt_contactName" runat="server" placeholder="Contact Name"></asp:TextBox>
        <br />
        <asp:TextBox ID="txt_contactNumber" runat="server" placeholder="Contact Number"></asp:TextBox>
        <br />
        <asp:DropDownList ID="ddl_countries" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddl_countries_SelectedIndexChanged"></asp:DropDownList>
        <br />
        <asp:DropDownList ID="ddl_states" runat="server" ></asp:DropDownList>
        <br />
        <br />
        <asp:Button ID="btn_save" runat="server" Text="Save" OnClick="btn_save_Click" />
    </div>
    </form>
</body>
</html>
