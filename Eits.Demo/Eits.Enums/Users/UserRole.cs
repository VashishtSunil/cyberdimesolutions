﻿
namespace Eits.Enums.Users
{
    public enum UserRole
    {
        SystemAdmin = 1,
        NormalUser = 2,
    }
}
