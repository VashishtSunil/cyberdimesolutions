﻿using System.Web;

namespace Eits.DomainLogic
{
    public static class UserVariables
    {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="index"></param>
        /// <param name="split"></param>
        /// <returns></returns>
        static object GetValue(int index, char split = '}')
        {
            string userData = HttpContext.Current.User.Identity.Name;
            string[] st = userData.Split(split);
            if (st.Length <= 0) throw new System.InvalidOperationException("Out of index");
            return st[index];
        }

        /// <summary>
        /// get loggedin user UserName ---- 0
        /// </summary>
        public static string UserName
        {
            get
            {
                return GetValue(0).ToString();
            }
        }
        /// <summary>
        /// Login User ID -- 1
        /// </summary>
        public static int UserId
        {
            get
            {
                return GetValue(1).ToType<int>();
            }
        }
        /// <summary>
        /// Login User Role ID --- 2
        /// </summary>
        public static short RoleId
        {
            get
            {
                return GetValue(2).ToType<short>();
            }
        }
        /// <summary>
        /// User Role
        /// </summary>
        public static Enums.Users.UserRole UserRole
        {
            get
            {
                return RoleId.ToEnum<Enums.Users.UserRole>();
            }
        }
        /// <summary>
        /// Check current user is login or not
        /// </summary>
        public static bool IsAuthenticated
        {
            get
            {
                if (HttpContext.Current == null) return false;
                return HttpContext.Current.User != null && HttpContext.Current.User.Identity.IsAuthenticated;
            }
        }
    }
}
