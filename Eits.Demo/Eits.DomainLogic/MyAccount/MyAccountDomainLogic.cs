﻿using Eits.DataAccess.MyAccount;
using Eits.Domain.Users;

namespace Eits.DomainLogic.MyAccount
{
    public class MyAccountDomainLogic : BaseClasses.DomainLogicBase
    {
        #region"My Account Domain Logic"
        /// <summary>
        /// Get Single Record of UserDetail By UserId
        /// </summary>
        /// <returns></returns>
        public Domain.Users.UserDetail GetUserProfileDetails()
        {
            using (MyAccountDataAccess obj = new MyAccountDataAccess())
            {
                return obj.GetUserProfileDetails(UserVariables.UserId);
            }
        }
        /// <summary>
        /// Update Record of UserDetail
        /// </summary>
        /// <param name="userDetail"></param>
        public void UpdateUserProfileDetails(UserDetail userDetail)
        {
            using (MyAccountDataAccess obj = new MyAccountDataAccess())
            {
                userDetail.UserId = UserVariables.UserId;
                obj.UpdateUserProfileDetails(userDetail);
            }
        }

        #endregion
    }
}
