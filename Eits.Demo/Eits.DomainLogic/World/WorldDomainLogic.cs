﻿using Eits.DataAccess.World;
using Eits.Domain.World;
using System.Collections.Generic;


namespace Eits.DomainLogic.World
{
    public class WorldDomainLogic : BaseClasses.DomainLogicBase
    {
        /// <summary>
        /// Get All Countries Details 
        /// </summary>
        /// <returns></returns>
        public static List<Country> GetCountry()
        {
            using (WorldDataAccess obj = new WorldDataAccess())
            {
                return obj.GetCountry();
            }
        }
        /// <summary>
        /// Get Related States of Country through CountryId
        /// </summary>
        /// <param name="countryId"></param>
        /// <returns></returns>
        public static List<State> GetState(int countryId)
        {
            using (WorldDataAccess obj = new WorldDataAccess())
            {
                return obj.GetState(countryId);
            }
        }
    }
}
