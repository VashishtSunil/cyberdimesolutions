﻿using Eits.DataAccess.Contacts;
using System.Collections.Generic;

namespace Eits.DomainLogic.Contacts
{
    public class ManageContactsDomainLogic : BaseClasses.DomainLogicBase
    {

        #region "Manage Contact Domain Logic"

        /// <summary>
        ///  Get List of Contact Records 
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <returns></returns>
        public List<Domain.MyContacts.Contacts> GetAllContacts(int pageIndex , int pagesize)
        {
            using (ManageContactsDataAccess obj = new ManageContactsDataAccess())
            {

                return obj.GetAllContacts(UserVariables.UserId, pageSize: pagesize, skip: SkipRecords(pageIndex , pagesize));
            }
        }
        /// <summary>
        ///  Add new contact
        /// </summary>
        /// <param name="contacts"></param>
        public void AddNewContact(Domain.MyContacts.Contacts contacts)
        {
            using (ManageContactsDataAccess obj = new ManageContactsDataAccess())
            {
                contacts.FKCreatedBy = UserVariables.UserId;
                obj.AddNewContact(contacts);
            }
        }
        /// <summary>
        /// Get Single Contact Record By ID
        /// </summary>
        /// <param name="contactId"></param>
        /// <returns></returns>
        public Domain.MyContacts.Contacts GetContactDetailsById(int contactId)
        {
            using (ManageContactsDataAccess obj = new ManageContactsDataAccess())
            {
                return obj.GetContactDetailsById(contactId, UserVariables.UserId);
            }
        }
        /// <summary>
        /// Update Single Contact Record 
        /// </summary>
        /// <param name="contacts"></param>
        public void UpdateContactDetails(Domain.MyContacts.Contacts contacts)
        {
            using (ManageContactsDataAccess obj = new ManageContactsDataAccess())
            {
                contacts.FkUpdatedBy = UserVariables.UserId;
                obj.UpdateContactDetails(contacts);
            }
        }
        /// <summary>
        /// Get Single Contact Record For Delete operation.
        /// </summary>
        /// <param name="contactId"></param>
        /// <returns></returns>
        public Domain.MyContacts.Contacts GetContactByDeleteConfirmation(int contactId)
        {
            using (ManageContactsDataAccess obj = new ManageContactsDataAccess())
            {

                return obj.GetContactByDeleteConfirmation(contactId);
            }
        }
        /// <summary>
        /// Delete the Single contact Record. 
        /// </summary>
        /// <param name="contactId"></param>
        public void DeleteSingleContact(int contactId)
        {
            using (ManageContactsDataAccess obj = new ManageContactsDataAccess())
            {

                obj.DeleteSingleContact(contactId);
            }
        }
        #endregion
    }
}
