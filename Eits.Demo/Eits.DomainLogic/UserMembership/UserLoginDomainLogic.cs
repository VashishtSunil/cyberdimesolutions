﻿

using Eits.DataAccess.UserMembership;
using Eits.Domain.Users;
using System;
using System.Globalization;
using System.Web;
using System.Web.Security;

namespace Eits.DomainLogic.UserMembership
{
    public class UserLoginDomainLogic : BaseClasses.DomainLogicBase
    {

        #region"User Login Domain Logic"

        /// <summary>
        /// User login
        /// </summary>
        /// <param name="userLogin"></param>
        /// <returns>If empty then user has logged in successfully else returns the message</returns>
        public string UserLogin(User userLogin)
        {
            using (UserLoginDataAccess objUserLogin = new UserLoginDataAccess())
            {
                //userLogin.UserPassword = Utilities.Encryption.Encryption.EncryptText(userLogin.UserPassword, userLogin.UserName);
                userLogin = objUserLogin.UserLogin(userLogin);
                if (userLogin == null) return "Please enter correct username and password";
            }
            UserTicket(userLogin);
            return string.Empty;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userLogin"></param>
        public void ForceFullUserLogin(User userLogin)
        {
            UserTicket(userLogin);
        }
        /// <summary>
        /// User Ticket Generate method
        /// </summary>
        /// <param name="userLoginDomain"></param>
        void UserTicket(User userLoginDomain)
        {
            string userData = userLoginDomain.UserName + "}" //--- 0
                            + userLoginDomain.UserId + "}" //---   1
                            + userLoginDomain.UserRoleId; //---    2
            FormsAuthenticationTicket tkt = new FormsAuthenticationTicket(1, userData, DateTime.Now, DateTime.Now.AddHours(5), false, userLoginDomain.UserRoleId.ToString(CultureInfo.InvariantCulture), FormsAuthentication.FormsCookiePath);
            string st = FormsAuthentication.Encrypt(tkt);
            HttpCookie ck = new HttpCookie(FormsAuthentication.FormsCookieName, st) { HttpOnly = true };
            HttpContext.Current.Response.Cookies.Add(ck);

        }

        #endregion
    }
}
