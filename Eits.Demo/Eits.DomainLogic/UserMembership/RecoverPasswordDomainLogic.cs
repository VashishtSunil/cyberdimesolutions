﻿using Eits.DataAccess.UserMembership;

namespace Eits.DomainLogic.UserMembership
{
    public class RecoverPasswordDomainLogic : BaseClasses.DomainLogicBase
    {
        #region "Recover Password Domain Logic"

        /// <summary>
        /// Recover password method
        /// </summary>
        /// <param name="recoverPassword"></param>
        /// <returns></returns>
        public bool RecoverPassword(Domain.UserMemberShip.RecoverPassword recoverPassword)
        {
            using (RecoverPasswordDataAccess objRecoverPassword = new RecoverPasswordDataAccess())
            {
                string emailAddress = objRecoverPassword.RecoverPassword(recoverPassword);
                if (string.IsNullOrEmpty(emailAddress)) return false;
                //send email to user for password recovery steps
                return true;
            }
        }

        #endregion
    }
}
