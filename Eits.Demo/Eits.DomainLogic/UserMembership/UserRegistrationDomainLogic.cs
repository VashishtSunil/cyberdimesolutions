﻿using Eits.DataAccess.UserMembership;
using Eits.Domain.Users;


namespace Eits.DomainLogic.UserMembership
{
    public class UserRegistrationDomainLogic : BaseClasses.DomainLogicBase
    {

        #region "Register Normal User Domain Logic"
        /// <summary>
        /// Normal User Register Method
        /// </summary>
        /// <param name="registerUser"></param>
        /// <returns></returns>
        public bool RegisterNormalUser(UserDetail registerUser)
        {
            using (UserRegistrationDataAccess obj = new UserRegistrationDataAccess())
            {
             //   registerUser.User.UserPassword = Utilities.Encryption.Encryption.EncryptText(registerUser.User.UserPassword, registerUser.User.UserName);
                registerUser.User.UserRoleId = Enums.Users.UserRole.NormalUser.ToType<short>();
                return obj.RegisterNormalUser(registerUser);
            }
        }

        #endregion
    }
}
