﻿ 

namespace Eits.DomainLogic.BaseClasses
{
    public abstract class DomainLogicBase
    {
        /// <summary>
        /// Records Skip 
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <returns></returns>
        protected int SkipRecords(int pageIndex , int pagesize)
        {
            return (pageIndex * pagesize);
        }
    
        /// <summary>
        /// Set Page Size
        /// </summary>
        //protected int PageSize
        //{
        //    get
        //    {
        //        return 100;
        //    }

        //}
    }
}
