﻿using System.Collections.Generic;
using System.Web.Script.Serialization;

namespace Utilities.Conversion
{
    public static class JavaScriptTypeCast
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dict"></param>
        /// <returns></returns>
        public static string DictionaryToJson(Dictionary<string, string> dict)
        {

            JavaScriptSerializer jss = new JavaScriptSerializer();
            return jss.Serialize(dict);
        }

    }
}
