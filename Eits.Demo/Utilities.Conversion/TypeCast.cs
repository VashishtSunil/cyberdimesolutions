﻿using System;
using System.ComponentModel.DataAnnotations;

public static class TypeCast
{
    public static T ToType<T>(this object val, T alt = default(T)) where T : struct, IConvertible
    {
        try
        {
            if (val == null) return alt;
            if (val is DBNull) return alt;
            return (T)Convert.ChangeType(val, typeof(T));
        }
        catch
        {
            return alt;
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="val"></param>
    /// <param name="alt"></param>
    /// <returns></returns>
    public static T? ToTypeOrNull<T>(this object val, T? alt = null) where T : struct, IConvertible
    {
        try
        {
            if (val != null && !(val is DBNull))
            {
                return (T)Convert.ChangeType(val, typeof(T));
            }
            return null;
        }
        catch
        {
            return alt;
        }
    }
    /// <summary>
    /// used to get Enum From String
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="value"></param>
    /// <returns></returns>
    public static T ToEnum<T>(this object value)
    {
        return (T)Enum.Parse(typeof(T), value.ToString(), true);
    }
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="value"></param>
    /// <returns></returns>
    public static string ToEnumString<T>(this object value)
    {
        value = (T)Enum.Parse(typeof(T), value.ToString(), true);
        var fieldInfo = value.GetType().GetField(value.ToString());
        var descriptionAttributes = fieldInfo.GetCustomAttributes(
            typeof(DisplayAttribute), false) as DisplayAttribute[];
        if (descriptionAttributes == null) return string.Empty;
        return (descriptionAttributes.Length > 0) ? descriptionAttributes[0].Name : value.ToString();
    }

}

