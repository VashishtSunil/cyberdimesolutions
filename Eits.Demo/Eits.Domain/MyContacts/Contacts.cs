﻿
using Eits.Domain.World;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Eits.Domain.MyContacts
{
    public class Contacts : BaseClasses.DomainBase
    {
        [Key]
        public int ContactId { get; set; }

        [StringLength(50, ErrorMessage = "Contact name cannot be longer than 50 characters")]
        [Required(ErrorMessage = "Please enter name")]
        [Column(TypeName = "varchar")]
        public string ContactPerson { get; set; }

        [StringLength(50, ErrorMessage = "Contact no. cannot be longer than 50 characters")]
        [Required(ErrorMessage = "Please enter contact no.")]
        [Column(TypeName = "varchar")]
        public string ContactNo { get; set; }

        [Required(ErrorMessage = "Please select state")]
        [ForeignKey("ContactState")]
        public int FkStateId { get; set; }

        public virtual State ContactState { get; set; }

        int _contactCountryId = 0;

        [Required(ErrorMessage = "Please select country")]
        [NotMapped]
        public int ContactCountryId
        {
            get
            {
                if (ContactState != null && ContactState.FKCountryId != 0)
                {
                    return ContactState.FKCountryId;
                }
                return _contactCountryId;
            }
            set { _contactCountryId = value; }
        }
        [NotMapped]
        public int TotalRecords { get; set; }
       
        [ForeignKey("FkCreatedByUser")]
        public int FKCreatedBy { get; set; }

        public virtual Users.User FkCreatedByUser { get; set; }

        [NotMapped]
        public System.Collections.Generic.List<Country> LstCountry { get; set; }

        [NotMapped]
        public System.Collections.Generic.List<State> LstState { get; set; }
    }
}
