﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Eits.Domain.BaseClasses
{
    public abstract class DomainBase
    {
        
        private bool _isActive = true;
        public bool IsActive
        {
            get { return _isActive; }
            set { _isActive = value; }
        }


        private bool _isDeleted = false;
        public bool IsDeleted
        {
            get { return _isDeleted; }
            set { _isDeleted = value; }
        }

        private DateTime _createdDateTime = DateTime.Now;
        public DateTime CreatedDateTime
        {
            get { return _createdDateTime; }
            set { _createdDateTime = value; }
        }

        [NotMapped]
        public int FkUpdatedBy { get; set; }
    }
}
