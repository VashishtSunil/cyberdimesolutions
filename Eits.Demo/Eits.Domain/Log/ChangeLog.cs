﻿
using Eits.Domain.BaseClasses;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Eits.Domain.Log
{
    public class ChangeLog : DomainBase
    {
        [Key]
        public int ChangeLogId { get; set; }

        [Column(TypeName = "varchar")]
        public string OriginalValue { get; set; }

        [Column(TypeName = "varchar")]
        public string NewValue { get; set; }

        public int RecordId { get; set; }

        [Required]
        [StringLength(50)]
        [Column(TypeName = "varchar")]
        public string EventType { get; set; }

        [Required]
        [StringLength(500)]
        [Column(TypeName = "varchar")]
        public string Model { get; set; }

        //[ForeignKey("User")]
        public int FkRecordUpdatedBy { get; set; }

        //public virtual User User { get; set; }

    }
}
