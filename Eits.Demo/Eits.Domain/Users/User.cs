﻿
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Eits.Domain.Users
{
    public class User : BaseClasses.DomainBase
    {
        [Key]
        public int UserId { get; set; }

        [StringLength(50, ErrorMessage = "User name cannot be longer than 50 characters")]
        [Required(ErrorMessage = "Please enter username")]
        [RegularExpression(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*", ErrorMessage = "Username must be a valid email address")]
        [Column(TypeName = "varchar")]
        public string UserName { get; set; }

        [StringLength(50, ErrorMessage = "User password cannot be longer than 50 characters")]
        [Required(ErrorMessage = "Please enter password")]
        [Column(TypeName = "varchar")]
        public string UserPassword { get; set; }

        [ForeignKey("UserRole")]
        public Int16 UserRoleId { get; set; }

        public virtual UserRole UserRole { get; set; }
    }
}
