﻿
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Eits.Domain.Users
{
    public class UserDetail : BaseClasses.DomainBase
    {

        [Key, ForeignKey("User")]
        public int UserId { get; set; }

        [StringLength(50, ErrorMessage = "First name cannot be longer than 50 characters")]
        [Required(ErrorMessage = "Please enter first name")]
        [Column(TypeName = "varchar")]
        public string FirstName { get; set; }

        [StringLength(50, ErrorMessage = "Last name cannot be longer than 50 characters")]
        [Required(ErrorMessage = "Please enter last name")]
        [Column(TypeName = "varchar")]
        public string LastName { get; set; }

        [StringLength(200, ErrorMessage = "Email cannot be longer than 200 characters")]
        [Required(ErrorMessage = "Please enter email address")]
        [RegularExpression(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*", ErrorMessage = "Please enter valid email address")]
        [Column(TypeName = "varchar")]
        public string EmailAddress { get; set; }


        public DateTime? DateOfBirth { get; set; }

        [StringLength(20, ErrorMessage = "Postcode cannot be longer than 20 characters")]
        [Column(TypeName = "varchar")]
        public string PostCode { get; set; }

        public virtual User User { get; set; }
        [Column(TypeName = "varchar")]
        public string OrignalFileName { get; set; }
        [Column(TypeName = "varchar")]
        public string FileName { get; set; }
    }
}
