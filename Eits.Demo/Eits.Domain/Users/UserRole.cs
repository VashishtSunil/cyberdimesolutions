﻿
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Eits.Domain.Users
{
    public class UserRole : BaseClasses.DomainBase
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Int16 UserRoleId { get; set; }

        [StringLength(50, ErrorMessage = "Role cannot be longer than 50 characters")]
        [Required(ErrorMessage = "Please enter role name")]
        [Column(TypeName = "varchar")]
        public string RoleName { get; set; }

        [StringLength(10, ErrorMessage = "Role code cannot be longer than 10 characters")]
        [Required(ErrorMessage = "Please enter user name")]
        [Column(TypeName = "varchar")]
        public string RoleCode { get; set; }

        [StringLength(500, ErrorMessage = "Role description cannot be longer than 500 characters")]
        [Column(TypeName = "varchar")]
        public string RoleDescription { get; set; }
    }
}
