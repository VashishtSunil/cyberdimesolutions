﻿
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Eits.Domain.World
{
    public class State : BaseClasses.DomainBase
    {
        [Key]
        public int StateId { get; set; }

        [StringLength(256, ErrorMessage = "State name be longer than 256 characters")]        
        [Column(TypeName = "varchar")]
        public string StateName { get; set; }

        [ForeignKey("StateCountry")]
        public int FKCountryId { get; set; }

        public virtual Country StateCountry { get; set; }
    }
}
