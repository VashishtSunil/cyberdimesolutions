﻿

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Eits.Domain.World
{
    public class Country : BaseClasses.DomainBase
    {
        [Key]
        public int CountryId { get; set; }

        [StringLength(256, ErrorMessage = "Country name be longer than 256 characters")]
        [Column(TypeName = "varchar")]
        public string CountryName { get; set; }

        public virtual List<State> LstCountryState { get; set; }
    }
}
