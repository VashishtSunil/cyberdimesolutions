﻿using System.ComponentModel.DataAnnotations;
namespace Eits.Domain.UserMemberShip
{
  public  class RecoverPassword : BaseClasses.DomainBase
    {
        [StringLength(50, ErrorMessage = "User name cannot be longer than 50 characters")]
        [Required(ErrorMessage = "Please enter username")]
        [RegularExpression(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*", ErrorMessage = "Username must be a valid email address")]
        public string UserName { get; set; }
    }
}
