﻿
using System.ComponentModel.DataAnnotations;
namespace MvcDemo.Domain.UserMemberShip
{
    public class UserRegistration : BaseClasses.DomainBase
    {
        [StringLength(50, ErrorMessage = "First name cannot be longer than 50 characters")]
        [Required(ErrorMessage = "Please enter first name")]
        public string FirstName { get; set; }

        [StringLength(50, ErrorMessage = "Last name cannot be longer than 50 characters")]
        [Required(ErrorMessage = "Please enter last name")]
        public string LastName { get; set; }

        [StringLength(50, ErrorMessage = "Username cannot be longer than 50 characters")]
        [Required(ErrorMessage = "Please enter user name")]
        [RegularExpression(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*", ErrorMessage = "Username must be a valid email address")]
        public string UserName { get; set; }

        [StringLength(50, ErrorMessage = "Password cannot be longer than 50 characters")]
        [Required(ErrorMessage = "Please enter password")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Please enter confirm password")]
        public string ConfirmPassword { get; set; }

        public short UserInRole { get; set; }
    }

   
}
