﻿using System.ComponentModel.DataAnnotations;

namespace MvcDemo.Domain.UserMemberShip
{
    public class UserLogin : BaseClasses.DomainBase
    {
        [StringLength(50, ErrorMessage = "User name cannot be longer than 50 characters")]
        [Required(ErrorMessage = "Please enter username")]
        [RegularExpression(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*", ErrorMessage = "Username must be a valid email address")]
        public string UserName { get; set; }

        [StringLength(50, ErrorMessage = "User password cannot be longer than 50 characters")]
        [Required(ErrorMessage = "Please enter password")]
        public string UserPassword { get; set; }

        public Enums.Users.UserRole UserInRole { get; set; }
    }
}