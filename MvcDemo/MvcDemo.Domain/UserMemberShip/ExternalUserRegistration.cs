﻿
using MvcDemo.Enums.Users;

namespace MvcDemo.Domain.UserMemberShip
{
    public class ExternalUserRegistration : BaseClasses.DomainBase
    {
        public ExternalProvider Provider { get; set; }
        public string ProviderUserId { get; set; }
        public string UserName { get; set; }
    }
}
