﻿
namespace MvcDemo.Domain
{
    public class State : BaseClasses.DomainBase
    {
        public int StateId { get; set; }
        public string StateName { get; set; }
        public int FkCountryId { get; set; }

        public Country StateCountry { get; set; }
    }
}
