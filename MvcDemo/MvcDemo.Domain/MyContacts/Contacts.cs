﻿
using System.ComponentModel.DataAnnotations;
namespace MvcDemo.Domain.MyContacts
{
    public class Contacts : BaseClasses.DomainBase
    {
        [StringLength(50, ErrorMessage = "Contact name cannot be longer than 50 characters")]
        [Required(ErrorMessage = "Please enter name")]
        public string ContactPerson { get; set; }

        [StringLength(50, ErrorMessage = "Contact no. cannot be longer than 50 characters")]
        [Required(ErrorMessage = "Please enter contact no.")]
        public string ContactNo { get; set; }

        public int ContactId { get; set; }

        public State ContactState { get; set; }

        [Required(ErrorMessage = "Please select state")]
        public int StateId { get; set; }

        public System.Collections.Generic.List<Country> LstCountry { get; set; }

        public System.Collections.Generic.List<State> LstState { get; set; }

    }
}
