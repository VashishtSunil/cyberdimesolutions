﻿
namespace MvcDemo.Domain
{
    public class Country : BaseClasses.DomainBase
    {
        public int CountryId { get; set; }
        public string CountryName { get; set; }
    }
}
