﻿using System.Web.Mvc;
using ControllerBase = MvcDemo.UI.Controllers.BaseClasses.ControllerBase;

namespace MvcDemo.UI.Controllers.BaseClasses
{
    [Authorize]
    [AdminUserModulesCheck]
    public abstract class AuthoriseUserControllerBase : ControllerBase
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected ActionResult RedirectAfterLogOut()
        {
            return RedirectToAction("userlogin", "usermembership");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    class AdminUserModulesCheck : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (System.Web.HttpContext.Current.Request.Url.ToString().Contains("/myaccount/logout")) return;
            //UserRole currentUserRole = DomainLogic.UserVariables.UserRole;
            //admin modules check
            //if (currentUserRole == UserRole.Sa) return;
           // string currentController = filterContext.RequestContext.RouteData.Values["controller"].ToString().ToLower();
            //switch (currentController)
            //{
            //    case "manageusers":           
            //        filterContext.Result = new RedirectResult("~/Error/Error404");
            //        break;
            //}
        }
    }
}