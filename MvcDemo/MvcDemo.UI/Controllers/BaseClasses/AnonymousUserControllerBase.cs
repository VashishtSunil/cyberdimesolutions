﻿
using System.Web.Mvc;
using ControllerBase = MvcDemo.UI.Controllers.BaseClasses.ControllerBase;

namespace MvcDemo.UI.Controllers.BaseClasses
{
    public abstract class AnonymousUserControllerBase : ControllerBase
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="returnUrl"></param>
        /// <returns></returns>
        protected ActionResult RedirectAfterLogin(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("editprofile", "myaccount");
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="returnUrl"></param>
        /// <returns></returns>
        protected ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("userlogin", "usermembership");
        }
    }
}