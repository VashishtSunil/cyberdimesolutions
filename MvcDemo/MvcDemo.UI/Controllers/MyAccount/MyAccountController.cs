﻿using System.Web.Mvc;
using System.Web.Security;

namespace MvcDemo.UI.Controllers.MyAccount
{
    public class MyAccountController : BaseClasses.AuthoriseUserControllerBase
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult EditProfile()
        {
            return View();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult LogOut()
        {
            FormsAuthentication.SignOut();
            return RedirectAfterLogOut();
        }
    }
}
