﻿using MvcDemo.DomainLogic;
using MvcDemo.DomainLogic.Contacts;
using System.Web.Mvc;

namespace MvcDemo.UI.Controllers.Contacts
{
    public class ContactsController : BaseClasses.AuthoriseUserControllerBase
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult MyContacts()
        {
            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public JsonResult GetContacts()
        {
            ManageContactsDomainLogic obj = new ManageContactsDomainLogic();
            return new JsonResult { Data = obj.GetAllContacts(), JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult SaveContact(int id = 0)
        {
            Domain.MyContacts.Contacts result;
            if (id > 0)
            {
                ManageContactsDomainLogic obj = new ManageContactsDomainLogic();
                result = obj.GetContactDetailsById(id);
                result.LstState = WorldDomainLogic.GetState(result.ContactState.FkCountryId);
            }
            else
            {
                result = new Domain.MyContacts.Contacts();
            }
            result.LstCountry = WorldDomainLogic.GetCountry();
            return View(result);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="countryId"></param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult GetStateList(int countryId)
        {
            return new JsonResult
            {
                Data = WorldDomainLogic.GetState(countryId),
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }
      /// <summary>
      /// 
      /// </summary>
      /// <param name="contacts"></param>
      
      /// <returns></returns>
        [ValidateAntiForgeryToken]
        [HttpPost]
        public JsonResult SaveContact(Domain.MyContacts.Contacts contacts)
        {
            if (ModelState.IsValid)
            {
                ManageContactsDomainLogic obj = new ManageContactsDomainLogic();
                if (contacts.ContactId > 0)
                {
                    obj.UpdateContactDetails(contacts);
                }
                else
                {
                    obj.AddNewContact(contacts);
                }
                return ReturnAjaxSuccessMessage("Contact saved successfully");
            }
            return ReturnAjaxModelError();
        }
    }
}
