﻿using System.Web.Mvc;
using MvcDemo.Domain.UserMemberShip;
using MvcDemo.DomainLogic.UserMembership;

namespace MvcDemo.UI.Controllers.UserMembership
{
    public partial class UserMemberShipController
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult RecoverPassword()
        {
            return View(new RecoverPassword());
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="recoverPassword"></param>
        /// <returns></returns>
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult RecoverPassword(RecoverPassword recoverPassword)
        {
            if (!ModelState.IsValid) return View(recoverPassword);
            RecoverPasswordDomainLogic obj=new RecoverPasswordDomainLogic();
            if (obj.RecoverPassword(recoverPassword))
            {
                Success("Instructions to setup new password is sent to your email address"); 
                ModelState.Clear();
                recoverPassword=new RecoverPassword();
            }
            else
            {
                ErrorBlock("Username does not exists");
            }
            return View(recoverPassword);
        }
    }
}