﻿using System.Web.Mvc;
using MvcDemo.Domain.UserMemberShip;
using MvcDemo.DomainLogic.UserMembership;

namespace MvcDemo.UI.Controllers.UserMembership
{
    public partial class UserMemberShipController
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult UserLogin()
        {
            return View(new UserLogin());
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userlogin"></param>
        /// <param name="returnUrl"></param>
        /// <returns></returns>
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult UserLogin(UserLogin userlogin, string returnUrl)
        {
            if (!ModelState.IsValid) return View(userlogin);
            UserLoginDomainLogic obj = new UserLoginDomainLogic();
            string result = obj.UserLogin(userlogin);
            if (!string.IsNullOrEmpty(result))
            {
                ErrorBlock(result);
            }
            else
            {
                return RedirectAfterLogin(returnUrl);
            }
            return View(userlogin);
        }
    }
}