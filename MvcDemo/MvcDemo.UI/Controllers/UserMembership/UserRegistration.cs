﻿using System.Web.Mvc;
using MvcDemo.DomainLogic.UserMembership;

namespace MvcDemo.UI.Controllers.UserMembership
{
    public partial class UserMemberShipController
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Register()
        {
            return View(new Domain.UserMemberShip.UserRegistration());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userRegistration"></param>
        /// <param name="returnUrl"></param>
        /// <returns></returns>
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Register(Domain.UserMemberShip.UserRegistration userRegistration, string returnUrl)
        {
            if (!ModelState.IsValid) return View(userRegistration);
            UserRegistrationDomainLogic objUserRegistration = new UserRegistrationDomainLogic();
            string result = objUserRegistration.RegisterNormalUser(userRegistration);
            if (!string.IsNullOrEmpty(result))
            {
                ErrorBlock(result);
            }
            else
            {
                return RedirectAfterLogin(returnUrl);
            }
            return View(userRegistration);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult _TermsAndConditions()
        {
            return PartialView("_TermsAndConditions");
        }
    }
}