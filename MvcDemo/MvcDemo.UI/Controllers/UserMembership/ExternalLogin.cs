﻿using System.Web.Mvc;
using DotNetOpenAuth.AspNet;
using Microsoft.Web.WebPages.OAuth;
using MvcDemo.Domain.UserMemberShip;
using MvcDemo.DomainLogic.UserMembership;
using MvcDemo.Enums.Users;

namespace MvcDemo.UI.Controllers.UserMembership
{
    public partial class UserMemberShipController
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="provider"></param>
        /// <param name="returnUrl"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult ExternalLogin(ExternalProvider provider, string returnUrl)
        {
            return new ExternalLoginResult(provider.ToString(), Url.Action("ExternalLoginCallback", new { ReturnUrl = returnUrl }));
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="returnUrl"></param>
        /// <returns></returns>
        public ActionResult ExternalLoginCallback(string returnUrl)
        {
            // Rewrite request before it gets passed on to the OAuth Web Security classes
           // GooglePlusClient.RewriteRequest();

            AuthenticationResult result = OAuthWebSecurity.VerifyAuthentication(Url.Action("ExternalLoginCallback", new { ReturnUrl = returnUrl }));
            if (!result.IsSuccessful)
            {
                return RedirectToAction("ExternalLoginFailure");
            }
            UserRegistrationDomainLogic objRegistration = new UserRegistrationDomainLogic();
            objRegistration.ExternalUserRegistration(new ExternalUserRegistration
            {
                Provider = result.Provider.ToEnum<ExternalProvider>(),
                ProviderUserId = result.ProviderUserId,
                UserName = result.UserName
            });
            return RedirectAfterLogin(returnUrl);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult ExternalLoginFailure()
        {
            return View();
        }
    }
    /// <summary>
    /// 
    /// </summary>
    internal class ExternalLoginResult : ActionResult
    {
        public ExternalLoginResult(string provider, string returnUrl)
        {
            Provider = provider;
            ReturnUrl = returnUrl;
        }

        public string Provider { get; private set; }
        public string ReturnUrl { get; private set; }

        public override void ExecuteResult(ControllerContext context)
        {
            OAuthWebSecurity.RequestAuthentication(Provider, ReturnUrl);
        }
    }
}