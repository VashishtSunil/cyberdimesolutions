﻿var contacts_panel;
$(document).ready(function () {
    contacts_panel = $("#contacts_panel");
    if (contacts_panel.length <= 0) return;
    //Populate Contact
    LoadContacts();


    $('body').on('change', '#ContactState_FkCountryId', function () {
        var countryId = $(this).val();
        LoadStates(countryId);
    });
});

/*load contacts*/
function LoadContacts() {
    contacts_panel.html('Loading Data...');
    GetData("/contacts/getcontacts", "", LoadContactsSuccess);
}
function LoadContactsSuccess(d) {
    if (d.length > 0) {
        var $data = $('<table></table>').addClass('table table-responsive table-striped');
        var header = "<thead><tr><th>Contact Person</th><th>Contact No</th><th>Country</th><th>State</th><th></th></tr></thead>";
        $data.append(header);

        $.each(d, function (i, row) {
            var $row = $('<tr/>');
            $row.append($('<td/>').html(row.ContactPerson));
            $row.append($('<td/>').html(row.ContactNo));
            $row.append($('<td/>').html(row.ContactState.StateCountry.CountryName));
            $row.append($('<td/>').html(row.ContactState.StateName));
            $row.append($('<td/>').html("<a href='/contacts/savecontact/" + row.ContactId + "' class='popup'>Edit</a>&nbsp;"));
            $data.append($row);
        });

        contacts_panel.html($data);
    } else {
        var $noData = $('<div/>').html('No Data Found!');
        contacts_panel.html($noData);
    }
}

//Casecade dropdown - Populate states of selected country
function LoadStates(countryId) {
    var $state = $('#StateId');
    $state.empty();
    $state.append($('<option></option>').val('').html('Please Wait...'));
    if (countryId == null || countryId == "") {
        $state.empty();
        $state.append($('<option></option>').val('').html('Select State'));
        return;
    }
    GetData("/contacts/getstatelist", { countryID: countryId }, LoadStatesSuccess);
}
function LoadStatesSuccess(result) {
    var $state = $('#StateId');
    $state.empty();
    $state.append($('<option></option>').val('').html('Select State'));
    $.each(result, function (i, val) {
        $state.append($('<option></option>').val(val.StateId).html(val.StateName));
    });
}

/*Add new contact success starts*/
function AddNewContactSucess(result) {
    if (result.IsSuccess == false) {
        errorBlock(result.OutPutMessage);
    } else {
        $dialog.dialog('close');
        LoadContacts();
        success(result.OutPutMessage);
    }
}
/*Add new contact success ends*/