﻿using Microsoft.Web.WebPages.OAuth;

namespace MvcDemo.UI
{
    public static class AuthConfig
    {
        public static void RegisterAuth()
        {
            // To let users of this site log in using their accounts from other sites such as Microsoft, Facebook, and Twitter,
            // you must update this site. For more information visit http://go.microsoft.com/fwlink/?LinkID=252166

            //OAuthWebSecurity.RegisterMicrosoftClient(
            //    clientId: "",
            //    clientSecret: "");

            OAuthWebSecurity.RegisterTwitterClient(
                consumerKey: Utilities.AppSettings.GetStringValue("TwitterConsumerKey"),
                consumerSecret: Utilities.AppSettings.GetStringValue("TwitterConsumerSecret"));

            OAuthWebSecurity.RegisterFacebookClient(
                 appId: Utilities.AppSettings.GetStringValue("FacebookAppId"),
                appSecret: Utilities.AppSettings.GetStringValue("FacebookAppSecret"));

            //OAuthWebSecurity.RegisterClient(new GooglePlusClient(
            //    "573976431588-tgr8ehv4a9daamnapfqn3ioueu5vcrg6.apps.googleusercontent.com", 
            //    System.Configuration.ConfigurationManager.AppSettings["ClientSecretKey"].ToString()), "google", null);
            //OAuthWebSecurity.RegisterGoogleClient();
        }
    }
}
