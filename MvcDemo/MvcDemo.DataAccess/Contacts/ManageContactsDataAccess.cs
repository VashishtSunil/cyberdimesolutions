﻿using System.Collections.Generic;
using System.Data;
using MvcDemo.Domain;

namespace MvcDemo.DataAccess.Contacts
{
    public class ManageContactsDataAccess : BaseClasses.DataAccessBase
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<Domain.MyContacts.Contacts> GetAllContacts()
        {
            Cmd.CommandText = "proc_contacts_getAllContacts";
            try
            {
                OpenConnection();
                Dr = Cmd.ExecuteReader();
                if (!Dr.HasRows) return null;
                List<Domain.MyContacts.Contacts> result = new List<Domain.MyContacts.Contacts>();
                while (Dr.Read())
                {
                    result.Add(new Domain.MyContacts.Contacts
                    {
                        ContactId = Dr["ContactId"].ToType<int>(),
                        ContactPerson = Dr["ContactPerson"].ToString(),
                        ContactNo = Dr["ContactNo"].ToString(),
                        ContactState = new State
                        {
                            StateName = Dr["StateName"].ToString(),
                            StateCountry = new Country
                            {
                                CountryName = Dr["CountryName"].ToString(),
                            }
                        }
                    });
                }
                return result;
            }
            finally
            {
                DisposeResources();
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="contacts"></param>
        public void AddNewContact(Domain.MyContacts.Contacts contacts)
        {
            Cmd.CommandText = "proc_contacts_AddNewContact";
            Cmd.Parameters.AddWithValue("@ContactPerson", contacts.ContactPerson);
            Cmd.Parameters.AddWithValue("@ContactNo", contacts.ContactNo);
            Cmd.Parameters.AddWithValue("@StateId", contacts.StateId);
            try
            {
                OpenConnection();
                Cmd.ExecuteNonQuery();
            }
            finally
            {
                DisposeResources();
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="contactId"></param>
        /// <returns></returns>
        public Domain.MyContacts.Contacts GetContactDetailsById(int contactId)
        {
            Cmd.CommandText = "proc_contacts_GetContactDetailsById";
            Cmd.Parameters.AddWithValue("@contactId", contactId);
            try
            {
                OpenConnection();
                Dr = Cmd.ExecuteReader();
                Dr.Read();
                return new Domain.MyContacts.Contacts
                {
                    ContactId = Dr["ContactId"].ToType<int>(),
                    ContactPerson = Dr["ContactPerson"].ToString(),
                    ContactNo = Dr["ContactNo"].ToString(),
                    ContactState = new State
                    {
                        FkCountryId = Dr["CountryId"].ToType<int>(),
                    },
                    StateId = Dr["StateId"].ToType<int>()
                };
            }
            finally
            {
                DisposeResources();
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="contacts"></param>
        public void UpdateContactDetails(Domain.MyContacts.Contacts contacts)
        {
            Cmd.CommandText = "proc_contacts_UpdateContactDetails";
            Cmd.Parameters.AddWithValue("@ContactId", contacts.ContactId);
            Cmd.Parameters.AddWithValue("@ContactPerson", contacts.ContactPerson);
            Cmd.Parameters.AddWithValue("@ContactNo", contacts.ContactNo);
            Cmd.Parameters.AddWithValue("@StateId", contacts.StateId);
            try
            {
                OpenConnection();
                Cmd.ExecuteNonQuery();
            }
            finally
            {
                DisposeResources();
            }
        }
    }
}
