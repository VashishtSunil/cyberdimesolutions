﻿using System.Collections.Generic;
using System.Data;
using MvcDemo.Domain;

namespace MvcDemo.DataAccess
{
    public class WorldDataAccess : BaseClasses.DataAccessBase
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<Country> GetCountry()
        {
            Cmd.CommandText = "proc_world_getAllCountries";
            try
            {
                OpenConnection();
                Dr = Cmd.ExecuteReader();
                List<Country> lstResult = new List<Country>();
                while (Dr.Read())
                {
                    lstResult.Add(new Country
                    {
                        CountryName = Dr["CountryName"].ToString(),
                        CountryId = Dr["CountryId"].ToType<int>(),
                    });
                }
                return lstResult;
            }
            finally
            {
                DisposeResources();
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="countryId"></param>
        /// <returns></returns>
        public List<State> GetState(int countryId)
        {
            Cmd.CommandText = "proc_world_getStateByCountryId";
            Cmd.Parameters.AddWithValue("@countryId", countryId);
            
            try
            {
                OpenConnection();
                Dr = Cmd.ExecuteReader();
                List<State> lstResult = new List<State>();
                while (Dr.Read())
                {
                    lstResult.Add(new State
                    {
                        StateName = Dr["StateName"].ToString(),
                        StateId = Dr["StateId"].ToType<int>(),
                    });
                }
                return lstResult;
            }
            finally
            {
                DisposeResources();
            }
        }
    }
}
