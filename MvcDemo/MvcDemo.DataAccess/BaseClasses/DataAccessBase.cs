﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace MvcDemo.DataAccess.BaseClasses
{
    public abstract class DataAccessBase : IDisposable
    {

        protected const string UserName = "admin@eits.com", Password = "admin";

        protected SqlConnection Con;
        protected SqlCommand Cmd;
        protected SqlDataReader Dr;
        /// <summary>
        /// 
        /// </summary>
        protected DataAccessBase()
        {
            Con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ConnectionString);
            Cmd = new SqlCommand { Connection = Con, CommandType = CommandType.StoredProcedure };
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="connectionString"></param>
        protected DataAccessBase(string connectionString)
        {
            Con = new SqlConnection(connectionString);
            Cmd = new SqlCommand { Connection = Con, CommandType = CommandType.StoredProcedure };
        }
        /// <summary>
        /// Open Connection
        /// </summary>
        protected void OpenConnection()
        {
            if (Con.State == ConnectionState.Closed)
            {
                Con.Open();
            }
        }

        #region Private Methods
        /// <summary>
        /// Close connection and remove its resources
        /// </summary>
        void DisposeConnection()
        {
            if (Con == null) return;
            Con.Close();
            Con.Dispose();
            Con = null;
        }
        /// <summary>
        /// Dispose Command
        /// </summary>
        void DisposeCommand()
        {
            if (Cmd == null) return;
            Cmd.Dispose();
            Cmd = null;
        }
        /// <summary>
        /// Dispose reader
        /// </summary>
        void DisposeReader()
        {
            if (Dr == null) return;
            Dr.Close();
            Dr.Dispose();
            Dr = null;
        }
        #endregion

        /// <summary>
        /// Dispose All Resources
        /// </summary>
        protected void DisposeResources()
        {
            DisposeConnection();
            DisposeCommand();
            DisposeReader();
        }
        /// <summary>
        /// Distructor
        /// </summary>
        ~DataAccessBase()
        {
            Dispose();
        }
        /// <summary>
        /// 
        /// </summary>
        public void Dispose()
        {
            DisposeResources();
            GC.SuppressFinalize(this);
        }
    }
}
