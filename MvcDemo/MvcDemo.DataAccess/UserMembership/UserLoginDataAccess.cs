﻿using MvcDemo.Enums.Users;

namespace MvcDemo.DataAccess.UserMembership
{
    public class UserLoginDataAccess : BaseClasses.DataAccessBase
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userLoginDomain"></param>
        /// <returns></returns>
        public Domain.UserMemberShip.UserLogin UserLogin(Domain.UserMemberShip.UserLogin userLoginDomain)
        {
            if (!userLoginDomain.UserName.ToLower().Equals(UserName) || !userLoginDomain.UserPassword.Equals(Password)) return null;

            userLoginDomain.UserInRole = UserRole.Na;
            return userLoginDomain;
        }


    }
}
