﻿
namespace MvcDemo.DataAccess.UserMembership
{
    public class RecoverPasswordDataAccess : BaseClasses.DataAccessBase
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="recoverPassword"></param>
        /// <returns></returns>
        public bool RecoverPassword(Domain.UserMemberShip.RecoverPassword recoverPassword)
        {
            return recoverPassword.UserName.ToLower().Equals(UserName);
        }
    }
}
