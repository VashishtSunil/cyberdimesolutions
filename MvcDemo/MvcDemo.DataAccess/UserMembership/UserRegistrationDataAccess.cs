﻿
using MvcDemo.Domain.UserMemberShip;

namespace MvcDemo.DataAccess.UserMembership
{
    public class UserRegistrationDataAccess : BaseClasses.DataAccessBase
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userRegistration"></param>
        /// <returns></returns>
        public UserRegistration RegisterNormalUser(UserRegistration userRegistration)
        {
            return userRegistration.UserName.ToLower().Equals(UserName) ? null : userRegistration;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="externalUserRegistration"></param>
        public void ExternalUserRegistration(ExternalUserRegistration externalUserRegistration)
        {

        }
    }
}
