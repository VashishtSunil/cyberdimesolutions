﻿
using MvcDemo.DataAccess.UserMembership;
using System;
using System.Web;
using System.Web.Security;
namespace MvcDemo.DomainLogic.UserMembership
{
    public class UserLoginDomainLogic : BaseClasses.DomainLogicBase
    {
        /// <summary>
        /// User login
        /// </summary>
        /// <param name="userLogin"></param>
        /// <returns>If empty then user has logged in successfully else returns the message</returns>
        public string UserLogin(Domain.UserMemberShip.UserLogin userLogin)
        {
            using (UserLoginDataAccess objUserLogin = new UserLoginDataAccess())
            {
                userLogin = objUserLogin.UserLogin(userLogin);
                if (userLogin == null) return "Please enter correct username and password";
            }
            UserTicket(userLogin);
            return string.Empty;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userLogin"></param>
        public void ForceFullUserLogin(Domain.UserMemberShip.UserLogin userLogin)
        {
            UserTicket(userLogin);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userLoginDomain"></param>
        void UserTicket(Domain.UserMemberShip.UserLogin userLoginDomain)
        {
            FormsAuthenticationTicket tkt = new FormsAuthenticationTicket(1, userLoginDomain.UserName, DateTime.Now, DateTime.Now.AddHours(5), false, userLoginDomain.UserInRole.ToString(), FormsAuthentication.FormsCookiePath);
            string st = FormsAuthentication.Encrypt(tkt);
            HttpCookie ck = new HttpCookie(FormsAuthentication.FormsCookieName, st) { HttpOnly = true };
            HttpContext.Current.Response.Cookies.Add(ck);
            
        }
    }
}
