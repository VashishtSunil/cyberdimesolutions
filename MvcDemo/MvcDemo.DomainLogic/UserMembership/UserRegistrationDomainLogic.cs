﻿
using MvcDemo.DataAccess.UserMembership;
using MvcDemo.Domain.UserMemberShip;

namespace MvcDemo.DomainLogic.UserMembership
{
    public class UserRegistrationDomainLogic : BaseClasses.DomainLogicBase
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userRegistartion"></param>
        /// <returns></returns>
        public string RegisterNormalUser(UserRegistration userRegistartion)
        {
            userRegistartion.UserInRole = Enums.Users.UserRole.Na.ToType<short>();
            using (UserRegistrationDataAccess obj = new UserRegistrationDataAccess())
            {
                userRegistartion = obj.RegisterNormalUser(userRegistartion);
            }
            if (userRegistartion == null)
            {
                return "Username already exists";
            }
            //making user login forcefully
            UserLoginDomainLogic objUserLogin=new UserLoginDomainLogic();
            objUserLogin.ForceFullUserLogin(new UserLogin
            {
                UserName = userRegistartion.UserName,
                UserInRole = Enums.Users.UserRole.Na
            });
            return string.Empty;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="externalUserRegistration"></param>
        public void ExternalUserRegistration(ExternalUserRegistration externalUserRegistration)
        {
            using (UserRegistrationDataAccess objRegistration=new UserRegistrationDataAccess())
            {
                objRegistration.ExternalUserRegistration(externalUserRegistration);
            }
            //making user login forcefully
            UserLoginDomainLogic objUserLogin = new UserLoginDomainLogic();
            objUserLogin.ForceFullUserLogin(new UserLogin
            {
                UserName = externalUserRegistration.ProviderUserId,
                UserInRole = Enums.Users.UserRole.Na
            });
        }
    }
}
