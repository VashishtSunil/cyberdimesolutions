﻿using MvcDemo.DataAccess.UserMembership;

namespace MvcDemo.DomainLogic.UserMembership
{
   public class RecoverPasswordDomainLogic : BaseClasses.DomainLogicBase
    {
       /// <summary>
       /// 
       /// </summary>
       /// <param name="recoverPassword"></param>
       /// <returns></returns>
       public bool RecoverPassword(Domain.UserMemberShip.RecoverPassword recoverPassword)
       {
           using (RecoverPasswordDataAccess objRecoverPassword=new RecoverPasswordDataAccess())
           {
               return objRecoverPassword.RecoverPassword(recoverPassword);
           }
       }
    }
}
