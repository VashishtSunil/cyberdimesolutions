﻿using MvcDemo.DataAccess.Contacts;
using System.Collections.Generic;

namespace MvcDemo.DomainLogic.Contacts
{
    public class ManageContactsDomainLogic : BaseClasses.DomainLogicBase
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<Domain.MyContacts.Contacts> GetAllContacts()
        {
            using (ManageContactsDataAccess obj = new ManageContactsDataAccess())
            {
                return obj.GetAllContacts();
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="contacts"></param>
        public void AddNewContact(Domain.MyContacts.Contacts contacts)
        {
            using (ManageContactsDataAccess obj = new ManageContactsDataAccess())
            {
                obj.AddNewContact(contacts);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="contactId"></param>
        /// <returns></returns>
        public Domain.MyContacts.Contacts GetContactDetailsById(int contactId)
        {
            using (ManageContactsDataAccess obj = new ManageContactsDataAccess())
            {
               return  obj.GetContactDetailsById(contactId);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="contacts"></param>
        public void UpdateContactDetails(Domain.MyContacts.Contacts contacts)
        {
            using (ManageContactsDataAccess obj = new ManageContactsDataAccess())
            {
                obj.UpdateContactDetails(contacts);
            }
        }
    }
}
