﻿using MvcDemo.DataAccess;
using MvcDemo.Domain;
using System.Collections.Generic;

namespace MvcDemo.DomainLogic
{
    public class WorldDomainLogic : BaseClasses.DomainLogicBase
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static List<Country> GetCountry()
        {
            using (WorldDataAccess obj = new WorldDataAccess())
            {
                return obj.GetCountry();
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="countryId"></param>
        /// <returns></returns>
        public static List<State> GetState(int countryId)
        {
            using (WorldDataAccess obj = new WorldDataAccess())
            {
                return obj.GetState(countryId);
            }
        }
    }
}
