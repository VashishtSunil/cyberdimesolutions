﻿namespace MvcDemo.Enums.Users
{
    public enum UserRole
    {
        /// <summary>
        /// Super Admin
        /// </summary>
        Sa = 1,
        /// <summary>
        /// Normal User
        /// </summary>

        Na = 2
    }
}
