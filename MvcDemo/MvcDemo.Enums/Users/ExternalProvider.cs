﻿

namespace MvcDemo.Enums.Users
{
    public enum ExternalProvider
    {
        Facebook,
        Twitter,
        Google,
    }
}
